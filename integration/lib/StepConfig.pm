#!perl

package StepConfig;
use strict;
use warnings;

use Data::Dumper;
use DBI;

use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(
                    $server
                    $resourcePaths
                    getResourcePath
                    $authCredentials
                    $requestData
                    $dbh
              );

our $server = 'http://ws/api';

our $authCredentials = {
    'notificationService' => {
                     'username' => 'NOTESERV_WS_USER',
                     'password' => 'notifyme',
                 },
};


our $resourcePaths = {
    'authentication' => '/authentication/v1',
    'sms'   => '/notification/v1/sms/message/{id}',
    'account'   => '/notification/v1/sms/account/{name}',
    'vendor'   => '/notification/v1/sms/vendor/{name}',
    'vendor-create'   => '/notification/v1/sms/vendor',
    'sender'   => '/notification/v1/sms/sender/{name}',
    'sender-create'   => '/notification/v1/sms/sender',
};

our $requestData = {
    'delete' => {
        'account' => {'Name' => 'bobsSMSAccount'},
    },
    'create' => {
        'doej-phone' => { 'data' =>
                            {
                                'PhoneNumber' => '937-123-1234',
                                'Sender' => 'passwordRecovery',
                                'Uuid' => 'doej',
                                'Body' => 'Some Text Message'
                            },
                            'dataType' => 'model',
                      },
        'doej-phone-bad-sender' => { 'data' =>
                              {
                                  'PhoneNumber' => '937-123-1234',
                                  'Sender' => 'passworRecovery',
                                  'Uuid' => 'doej',
                                  'Body' => 'Some Text Message'
                              },
                              'dataType' => 'model',
                        },
        'password-sender' => { 'data' =>
                            { 'Name' => 'passwordRecovery',
                            },
                            'dataType' => 'model',
                        },
        'password-account' => { 'data' =>
                            { 'Name' => 'bobsSMSAccount',
                              'Vendor' => 'bobsSMS',
                            },
                            'dataType' => 'model',
                        },
        'password-vendor' => { 'data' =>
                            { 'Name' => 'bobsSMS',
                            'Classname' => '\MiamiOH\NotificationService\Services\Vendor\BobsSMS',
                            'Endpointlocation' => '/bobssms'
                            },
                            'dataType' => 'model',
                        },

    }
};

sub getResourcePath {
    my $pathKey = shift;
    my $values = shift;

    my $path = '';

    if (defined($resourcePaths->{$pathKey})) {
        $path = $resourcePaths->{$pathKey};

        if ($values && ref($values) eq 'HASH') {
            foreach my $key ( keys %{$values}) {
                $path =~ s/\{$key\}/$values->{$key}/g;
            }
        }
    }

    return $path;
}

our $dbh = DBI->connect("DBI:Oracle:XE", "notsrv", "Hello123") || die DBI->errstr;


1;