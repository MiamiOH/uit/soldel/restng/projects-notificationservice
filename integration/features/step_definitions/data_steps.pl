#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/the test data is ready/, sub {

    foreach my $table (qw(notsrv_email)) {
        $dbh->do(qq{
                delete from $table
            });
    }

    $dbh->do(q{
        drop sequence notsrv_notification_id
        });

    $dbh->do(q{
        create sequence notsrv_notification_id
        });

};

Given qr/(\S+) sample (\S+) records are loaded/, sub {
    my $recordCount = $1;
    my $recordType = $2;

    if ($recordType eq 'email') {
        my %ratio = (
                '1' => .30,
                '2' => .25,
                '3' => .20,
                '4' => .15,
                '5' => .10,
            );


        my %samplePoints;

        foreach my $p (sort keys %ratio) {
            $samplePoints{$p} = ($ratio{$p} * $recordCount) + ($p > 1 ? $samplePoints{$p - 1} : 0);
        }

        for (my $i = 0; $i < $recordCount; $i++) {

            my $priority = 0;

            if ($i < $samplePoints{1}) { 
               $priority = 1; 
            } elsif ($i < $samplePoints{2}) { 
                $priority = 2;
            } elsif ($i < $samplePoints{3}) { 
                $priority = 3; 
            } elsif ($i < $samplePoints{4}) { 
                $priority = 4; 
            } else {
                $priority = 5;
            }

            $dbh->do(q{
                insert into notsrv_email (id, status, priority, uuid, to_addr, from_addr, subject, body, 
                        start_date, scheduled_date, activity_date)
                    values (notsrv_notification_id.nextval, ?, ?, ?, ?, ?, ?, ?, sysdate, sysdate, sysdate)
                }, undef, 'P', $priority, 'doej' . $i, 'doej' . $i . '@example.com', 'info@example.com',
                    'Test alert message', 'This should have stuff for #' . $i . ' in it.');
        }
    }

};

