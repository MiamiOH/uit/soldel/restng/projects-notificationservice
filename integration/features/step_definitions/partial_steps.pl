#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use JSON;

use lib 'lib';
use StepConfig;

When qr/I want to get fields "([^"]+)"/, sub {
    S->{'fields'} = $1;
};

When qr/I want to get subobjects "([^"]+)"/, sub {
    S->{'fields'} = $1;
};

