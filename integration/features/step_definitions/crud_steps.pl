#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use lib 'lib';
use StepConfig;

Given qr/the (\S+) (\S+) data loaded from (\S+)/, sub {
    my $action = $1;
    my $element = $3;
    my $modelType = $2;

    my $token = getToken();
    my $name = $requestData->{$action}{$element}{'data'}{'Name'};
    my $payload = getResourceContent(getResourcePath($modelType, { 'name' => $name }) . "?token=$token");
    my $createNeeded = 'false';
    if  (ref($payload->{'data'}) eq 'ARRAY') {
        if(scalar(@{$payload->{'data'}}) != 0) {
           ok(($payload->{'data'}{'Name'}) eq $name, "{$modelType} $name exists");
        } else {
          $createNeeded = 'true';
        }
    }
    elsif ($payload->{'data'}{'Name'} eq $requestData->{'create'}{$element}{'data'}{'Name'})  {
            ok($payload->{'data'}{'Name'} eq $requestData->{'create'}{$element}{'data'}{'Name'});
    }
    else {
        $createNeeded = 'true';
    }
    if ($createNeeded eq 'true') {
        my $createResponse = createResourceResponse(getResourcePath($modelType.'-create') . "?token=$token",
            $requestData->{'create'}{$element});
        ok($createResponse->code() == 201, "Created $element record for $name");
    }
};

Given qr/a (\S+) exists for (\S+)/, sub {
    my $modelType = $1;
    my $name = $2;

    my $token = getToken();

    my $payload = getResourceContent(getResourcePath($modelType, { 'name' => $name }) . "?token=$token");
    ok(($payload->{'data'}{'Name'}) eq $name, "Existing $modelType object for $name");

};

Given qr/a (\S+) does not exists for (\S+)/, sub {
    my $modelType = $1;
    my $name = $2;
    my $token = getToken();
    my $deleteRecord = 'false';

    my $payload = getResourceContent(getResourcePath($modelType, { 'name' => $name }) . "?token=$token");
    if  (ref($payload->{'data'}) eq 'ARRAY') {
        if(scalar(@{$payload->{'data'}}) != 0) {
            $deleteRecord = 'true';
        } else {
           ok((scalar(@{$payload->{'data'}}) == 0), "{$modelType} $name does not exists");
        }
    }
    else  {
        $deleteRecord = 'true';
    }

    if ($deleteRecord eq 'true') {
        my $deleteResponse = deleteResourceResponse(getResourcePath($modelType,
                { 'name' => $name }) . "?token=$token");
            ok($deleteResponse->code() == 200, "Delete existing $modelType object for $name");
        }
     else {
        ok(scalar(@{$payload->{'data'}}) == 0, "No existing $modelType object for $name");
    }
};


Given qr/an empty (\S+) table/, sub { 
    $dbh->do(qq{ delete from $1 });
};

# a book with the title "My Book"
Given qr/a (\S+) record with the (\S+) "([^"]+)"/, sub {
    S->{'recordType'} = $1;
    S->{'record'}{$2} = $3;
};

# that the book has been added
Given qr/that the (\S+) record has been added/, sub {
    ok($1 eq S->{'recordType'}, "Create a new $1");

    if ($1 eq S->{'recordType'}) {
        my $fields = join(', ', keys %{S->{'record'}});
        my $values = join(', ', map { "'" . $_ . "'"} values %{S->{'record'}});
        my $query = "insert into " . S->{'recordType'} . '(' . $fields . ') values (' . $values . ')';

        $dbh->do($query);
    }
};

Given qr/a (\S+) record with (\S+) (\S+) where (\S+) equals (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $withField = $4;
    my $withValue = $5;
    my $spec = $6;
    my $status = $6 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{ 
        select count(*)
            from $table
            where $field = ?
              and $withField = ?
        }, undef, $value, $withValue);

    ok($realStatus == $status, "A $table with $field of $value $spec where $withField equals $withValue");

};

Given qr/that a (\S+) record with (\S+) (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $spec = $4;
    my $status = $4 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{ 
        select count(*)
            from $table
            where $field = ?
        }, undef, $value);

    ok($realStatus == $status, "A $table with $field of $value $spec");

};

When qr/I have a (\S+) with the (\S+) "([^"]+)"/, sub {
    S->{'objectType'} = $1;
    S->{'object'}{$2} = $3;
};

When qr/I have a (\S+) model with the (\S+) "([^"]+)"/, sub {
    S->{'objectType'} = $1;
    S->{'model'}{$2} = $3;
};

When qr/I have a (\S+) model with the (\S+) given by calling (.*)/, sub {
    S->{'objectType'} = $1;
    eval(qq{S->{'model'}{$2} = $3});
};

When qr/I construct a model payload/, sub {
    S->{'object'} = {
        'dataType' => 'model',
        'data' => S->{'model'}
    }
};

Then qr/a (\S+) record with (\S+) (\S+) where (\S+) equals (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $withField = $4;
    my $withValue = $5;
    my $spec = $6;
    my $status = $6 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{ 
        select count(*)
            from $table
            where $field = ?
              and $withField = ?
        }, undef, $value, $withValue);

    ok($realStatus == $status, "A $table with $field of $value $spec where $withField equals $withValue");

};

Then qr/a (\S+) record with (\S+) (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $spec = $4;
    my $status = $4 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{ 
        select count(*)
            from $table
            where $field = ?
        }, undef, $value);

    ok($realStatus == $status, "A $table with $field of $value $spec");

};