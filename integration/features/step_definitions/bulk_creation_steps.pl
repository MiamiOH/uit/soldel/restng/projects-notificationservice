#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use lib 'lib';
use StepConfig;

When qr/I have (\S+) (\S+) objects/, sub {
    my $count = $1;
    S->{'objectType'} = $2;
    
    my $payload = {
        'dataType' => 'collection',
        'data' => []
    };

    for (1..$count) {
        my $object = {
            'toAddr' => 'doej@example.com',
            'body' => 'Check your vacation balance',
            'subject' => 'Important notice',
            'fromAddr' => 'info@example.com'

        };

        push(@{$payload->{'data'}}, $object);
    }

    S->{'object'} = $payload;
    S->{'testObjects'}{S->{'objectType'}} = $payload->{'data'};
};

Then qr/all (\S+) objects were created/, sub{
    my $type = $1;
    my $sourceData = S->{'testObjects'}{$1};

    my $body = from_json(${S->{'response'}->content_ref()});
    
    for (my $i = 0; $i < scalar(@{$sourceData}); $i++) {
        ok($body->{'data'}[$i]{'code'} == 201, "Object $i was created");
        ok(defined($body->{'data'}[$i]{'model'}{'id'}), "Object $i was created with an id");
    }
};


