#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use lib 'lib';
use StepConfig;

When qr/I have (\S+) existing pending (\S+) objects/, sub {
    my $count = $1;
    S->{'objectType'} = $2;
    
    S->{'sampleObjects'} = [];

    for (my $i = 0; $i < $count; $i++) {
        my $object = {
            'toAddr' => 'doej' . $i . '@example.com',
            'fromAddr' => 'info@example.com',
            'priority' => '3',
            'status' => 'P',
            'uid' => 'doej' . $i,
            'subject' => 'Important notice',
            'body' => 'Check your vacation balance',

        };

        push(@{S->{'sampleObjects'}}, $object);
        
        $dbh->do(q{
            insert into notsrv_email (id, status, priority, uuid, to_addr, from_addr, subject, body, 
                    start_date, scheduled_date, activity_date)
                values (notsrv_notification_id.nextval, ?, ?, ?, ?, ?, ?, ?, sysdate, sysdate, sysdate)
            }, undef, $object->{'status'}, $object->{'priority'}, $object->{'uid'}, $object->{'toAddr'}, 
                $object->{'fromAddr'}, $object->{'subject'}, $object->{'body'});
    }

    S->{'object'} = S->{'sampleObjects'};

};

#all (\S+) objects were updated to sent
Then qr/all (\S+) objects were updated to sent/, sub{
    my $type = $1;
    my $sourceData = S->{'sampleObjects'};

    my $body = from_json(${S->{'response'}->content_ref()});

    for (my $i = 0; $i < scalar(@{$sourceData}); $i++) {
        #ok($body->{'data'}[$i]{'code'} == 200, "Object $i was updated");
        #ok($body->{'data'}[$i]{'model'}{'status'} eq 'S', "Object $i was updated to sent");
    }
};
