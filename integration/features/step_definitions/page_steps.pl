#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use JSON;

use lib 'lib';
use StepConfig;

When qr/I set an offset of (\d+)/, sub {
    S->{'offset'} = $1;
};

When qr/I set a limit of (\d+)/, sub {
    S->{'limit'} = $1;
};

Then qr/the response contains records (\d+) to (\d+)/, sub {
    my $expectedFirstRecordNum = $1;
    my $expectedLastRecordNum = $2;

    my $body = from_json(${S->{'response'}->content_ref()});

    my $firstRecordNum = 0;
    my $lastRecordNum = 0;

    for (my $i = 0; $i < scalar(@{$body->{'data'}}); $i++) {
        if (!$firstRecordNum || $body->{'data'}[$i]{'id'} < $firstRecordNum) {
            $firstRecordNum = $body->{'data'}[$i]{'id'}
        }
        if (!$lastRecordNum || $body->{'data'}[$i]{'id'} > $lastRecordNum) {
            $lastRecordNum = $body->{'data'}[$i]{'id'}
        }
    }
    
    ok($firstRecordNum == $expectedFirstRecordNum, "First record $firstRecordNum equals expected $expectedFirstRecordNum");
    ok($lastRecordNum == $expectedLastRecordNum, "Last record $lastRecordNum equals expected $expectedLastRecordNum");
};
