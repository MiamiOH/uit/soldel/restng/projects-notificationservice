#!perl

use strict;
use warnings;
use LWP;
use JSON;

use lib 'lib';
use StepConfig;

use lib $ENV{'PHERKINSTEPS_HOME'};
use REST::Basic;
use REST::RequestData;
use REST::ResponseData;
use RESTng::PartialResponses;
use RESTng::PagedResponses;

# resourcePaths and authCredentials come from our StepConfig
sub getToken {

    my $ua = LWP::UserAgent->new();

    my $request = HTTP::Request->new('POST', $server . $resourcePaths->{'authentication'});

    my $data = {
        'username' => $authCredentials->{'notificationService'}{'username'},
        'password' => $authCredentials->{'notificationService'}{'password'},
        'type' => 'usernamePassword'
    };

    $request->content(to_json($data));
    $request->header('Content-Type', 'application/json');

    my $response = $ua->simple_request($request);

    my $tokenInfo = from_json(${$response->content_ref()});

    return $tokenInfo->{'data'}{'token'};
}

sub resourceResponse {
    my $method = shift;
    my $path = shift;
    my $data = shift || {};

    my $ua = LWP::UserAgent->new();

    my $request = HTTP::Request->new($method, $server . $path);

    if ($method eq 'POST') {
        $request->content(to_json($data));
        $request->header('Content-Type', 'application/json');
    }

    my $response = $ua->simple_request($request);

    return $response;
}

sub getResourceResponse {
    my $path = shift;

    my $response = resourceResponse('GET', $path);

    return $response;
}

sub deleteResourceResponse {
    my $path = shift;

    my $response = resourceResponse('DELETE', $path);

    return $response;
}

sub createResourceResponse {
    my $path = shift;
    my $data = shift;

    my $response = resourceResponse('POST', $path, $data);

    return $response;
}

sub getResourceContent {
    my $path = shift;

    my $response = getResourceResponse($path);

    my $data = from_json(${$response->content_ref()});
}

sub createLargeEmailBody {
    my $lines = shift;

    my $body = '';

    for (1..$lines) {
        $body .= "This is a line in a long email body\n";
    }

    return $body;
}