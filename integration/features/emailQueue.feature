# Test access to the Notification Email Queue Service REST API
Feature: Email notification queue resources
 As a consumer of the Notification Service Email Queue
 I want to fetch objects from the queue
 In order to process queue entries

Background:
  Given the test data is ready
  And 100 sample email records are loaded

Scenario: Get the queue entries
  Given a REST client
  And a token for the notificationService user
  When I make a GET request for /notification/v1/email/queue
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data array element contains 100 entries
  And the response data array element contains 30 entries with a priority key matching "1"
  And the response data array element contains 25 entries with a priority key matching "2"
  And the response data array element contains 20 entries with a priority key matching "3"
  And the response data array element contains 15 entries with a priority key matching "4"
  And the response data array element contains 10 entries with a priority key matching "5"
