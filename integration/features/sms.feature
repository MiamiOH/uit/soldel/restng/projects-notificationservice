# Test access to the Notification SMS Service REST API
Feature: SMS notification object resources
  As a consumer of the Notification Service
  I want to create, read and update SMS objects
  In order to manage the object state

  Scenario: Require authentication to use the SMS message service
    Given a REST client
    When I make a GET request for /notification/v1/sms/message/1
    Then the HTTP status code is 401

  Scenario: Require authentication to use the SMS message service
    Given a REST client
    When I make a POST request for /notification/v1/sms/message
    Then the HTTP status code is 401

  Scenario: Get an SMS notification
    Given a REST client
    And a token for the notificationService user
    When I make a GET request for /notification/v1/sms/message/1
    Then the HTTP status code is 200

    # This test is failing due to missing account sender data
#  Scenario: Create a new SMS notification using JSON data
#    Given a REST client
#    And a token for the notificationService user
#    And a sender exists for passwordRecovery
#    And the data to create doej-phone
#    When I give the HTTP Content-type header with the value "application/json"
#    And I make a POST request for /notification/v1/sms/message
#    Then the HTTP status code is 201

  Scenario: Create a new SMS notification using JSON data
    Given a REST client
    And a token for the notificationService user
    And a sender exists for passwordRecovery
    And the data to create doej-phone-bad-sender
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /notification/v1/sms/message
    Then the HTTP status code is 500
