# Test access to the Notification SMS Service REST API
Feature: SMS Account object resources
  As a consumer of the SMS Account Service
  I want to create, read and update SMS Account objects
  In order to manage the object state

  Scenario: Require authentication to use the SMS Account service
    Given a REST client
    When I make a GET request for /notification/v1/sms/account/1
    Then the HTTP status code is 401

  Scenario: Require authentication to use the SMS Account service
    Given a REST client
    When I make a POST request for /notification/v1/sms/account
    Then the HTTP status code is 401

  Scenario: Get an SMS Account
    Given a REST client
    And a token for the notificationService user
    When I make a GET request for /notification/v1/sms/account/1
    Then the HTTP status code is 200

  Scenario: Create a new SMS Account using JSON data
    Given a REST client
    And a token for the notificationService user
    And the data to create password-account
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /notification/v1/sms/account
    Then the HTTP status code is 201

  Scenario: Try to create a SMS Account using JSON data that is already there
    Given a REST client
    And a token for the notificationService user
    And a account exists for bobsSMSAccount
    And the data to create password-account
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /notification/v1/sms/account
    Then the HTTP status code is 500

  Scenario: Delete an SMS Account
    Given a REST client
    And the create account data loaded from password-account
    And a token for the notificationService user
    When I make a DELETE request for /notification/v1/sms/account/bobsSMSAccount
    Then the HTTP status code is 200
