# Test access to the Notification Email Collection Service REST API
Feature: Email notification collection resources
 As a consumer of the Notification Service Email Collection
 I want to fetch objects from the collection
 In order to process collection entries

Background:
  Given the test data is ready
  And 100 sample email records are loaded

Scenario: Get a REST endpoint
  Given a REST client
  And a token for the notificationService user
  When I make a GET request for /notification/v1/email/collection
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"

 Scenario: Requesting offset and limit result in the correct response elements
   Given a REST client
   And a token for the notificationService user
   When I set an offset of 1
   And I set a limit of 10
   And I make a GET request for /notification/v1/email/collection
   Then the HTTP status code is 200
   And the response contains a total element matching "100"
   And the response contains a currentUrl element matching "limit=10&offset=1"
   And the response contains a firstUrl element matching "limit=10&offset=1"
   And the response contains a nextUrl element matching "limit=10&offset=11"

 Scenario: Requesting specific fields results in only those fields being returned
   Given a REST client
   And a token for the notificationService user
   When I want to get fields "id,toAddr,fromAddr,subject"
   And I make a GET request for /notification/v1/email/collection
   Then the HTTP status code is 200
   And the response data element first entry contains a id key that is not empty
   And the response data element first entry contains a toAddr key that is not empty
   And the response data element first entry contains a fromAddr key that is not empty
   And the response data element first entry contains a subject key that is not empty
   And the response data element first entry does not contain a body key

