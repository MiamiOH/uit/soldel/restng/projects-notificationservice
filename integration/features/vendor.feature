# Test access to the Notification SMS Service REST API
Feature: SMS Vendor object resources
  As a consumer of the Notification Service
  I want to create, read and update SMS Vendor objects
  In order to manage the object state

  Scenario: Require authentication to use the SMS Vendor service
    Given a REST client
    When I make a GET request for /notification/v1/sms/vendor/1
    Then the HTTP status code is 401

  Scenario: Require authentication to use the SMS Vendor service
    Given a REST client
    When I make a POST request for /notification/v1/sms/vendor
    Then the HTTP status code is 401

  Scenario: Get an SMS Vendor
    Given a REST client
    And a token for the notificationService user
    When I make a GET request for /notification/v1/sms/vendor/1
    Then the HTTP status code is 200

  Scenario: Delete an SMS Vendor
    Given a REST client
    And the create vendor data loaded from password-vendor
    And a token for the notificationService user
    When I make a DELETE request for /notification/v1/sms/vendor/BobSMS
    Then the HTTP status code is 200

  Scenario: Create a new SMS Vendor using JSON data
    Given a REST client
    And a token for the notificationService user
    And a vendor does not exists for bobsSMS
    And the data to create password-vendor
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /notification/v1/sms/vendor
    Then the HTTP status code is 201


