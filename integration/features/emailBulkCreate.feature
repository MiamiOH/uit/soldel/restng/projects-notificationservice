# Test access to the Notification Email Service REST API
Feature: Email notification bulk create
 As a consumer of the Notification Service
 I want to create mulitple email notifacations with a single call
 In order to efficiently create notifications

Background:
  Given the test data is ready

Scenario: Get a REST endpoint
  Given a REST client
  And a token for the notificationService user
  When I have 10 emailNotification objects
  And I give the HTTP Content-type header with the value "application/json"
  And I make a POST request for /notification/v1/email/message
  Then the HTTP status code is 201
  And all emailNotification objects were created
