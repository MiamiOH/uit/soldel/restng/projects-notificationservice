# Test access to the Notification Email Service REST API
Feature: Email notification object resources
 As a consumer of the Notification Service
 I want to create, read and update email objects
 In order to manage the object state

Background:
  Given the test data is ready
  And 1 sample email records are loaded

Scenario: Require authentication to use the email message service
  Given a REST client
  When I make a GET request for /notification/v1/email/message/1
  Then the HTTP status code is 401

Scenario: Get a REST endpoint
  Given a REST client
  And a token for the notificationService user
  When I make a GET request for /notification/v1/email/message/1
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element contains a id key matching "1"

Scenario: An invalid id results in a not found response
  Given a REST client
  And a token for the notificationService user
  When I make a GET request for /notification/v1/email/message/10001
  Then the HTTP status code is 404

 Scenario: Create a new email notification using JSON data
   Given a REST client
   And a token for the notificationService user
   And that a notsrv_email record with id 2 does not exist
   When I have a emailNotification model with the toAddr "doej@example.com"
   And I have a emailNotification model with the fromAddr "info@example.com"
   And I have a emailNotification model with the subject "Important notice"
   And I have a emailNotification model with the priority "4"
   And I have a emailNotification model with the body "Check your vacation balance"
   And I construct a model payload
   And I give the HTTP Content-type header with the value "application/json"
   And I make a POST request for /notification/v1/email/message
   Then the HTTP status code is 201
   And a notsrv_email record with id 1 does exist
   And the response data element contains a id key matching "2"
   And the response data element contains a toAddr key matching "doej@example.com"
   And the response data element contains a fromAddr key matching "info@example.com"
   And the response data element contains a subject key matching "Important notice"
   And the response data element contains a body key matching "Check your vacation balance"
   And the response data element contains a priority key matching "4"

 Scenario: Create a new email notification with a large body
   Given a REST client
   And a token for the notificationService user
   And that a notsrv_email record with id 2 does not exist
   When I have a emailNotification model with the toAddr "doej@example.com"
   And I have a emailNotification model with the fromAddr "info@example.com"
   And I have a emailNotification model with the subject "Important notice"
   And I have a emailNotification model with the priority "4"
   And I have a emailNotification model with the body given by calling createLargeEmailBody(1000)
   And I construct a model payload
   And I give the HTTP Content-type header with the value "application/json"
   And I make a POST request for /notification/v1/email/message
   Then the HTTP status code is 201
   And a notsrv_email record with id 2 does exist
   And the response data element contains a id key matching "2"
   And the response data element contains a toAddr key matching "doej@example.com"
   And the response data element contains a fromAddr key matching "info@example.com"
   And the response data element contains a subject key matching "Important notice"
   And the response data element contains a body key that is not empty
   And the response data element contains a priority key matching "4"

 Scenario: Update an existing email notification using JSON data
   Given a REST client
   And a token for the notificationService user
   When I have a emailNotification with the id "1"
   And I have a emailNotification with the status "S"
   And I have a emailNotification with the toAddr "doej@example.com"
   And I have a emailNotification with the fromAddr "info@example.com"
   And I have a emailNotification with the subject "Important notice"
   And I have a emailNotification with the body "Check your vacation balance"
   And I give the HTTP Content-type header with the value "application/json"
   And I make a PUT request for /notification/v1/email/message/1
   Then the HTTP status code is 200

 Scenario: Requesting specific fields results in only those fields being returned
   Given a REST client
   And a token for the notificationService user
   When I want to get fields "id,toAddr,fromAddr,subject"
   And I make a GET request for /notification/v1/email/message/1
   Then the HTTP status code is 200
   And the response data element contains a id key that is not empty
   And the response data element contains a toAddr key that is not empty
   And the response data element contains a fromAddr key that is not empty
   And the response data element contains a subject key that is not empty
   And the response data element does not contain a body key
