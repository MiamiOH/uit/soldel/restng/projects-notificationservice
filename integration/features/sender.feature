# Test access to the Notification SMS Service REST API
Feature: SMS Sender object resources
  As a consumer of the SMS Sender Service
  I want to create, read and update SMS Sender objects
  In order to manage the object state

  Scenario: Require authentication to use the SMS Sender service
    Given a REST client
    When I make a GET request for /notification/v1/sms/sender/passwordRecovery
    Then the HTTP status code is 401

  Scenario: Require authentication to use the SMS Purpose service
    Given a REST client
    When I make a POST request for /notification/v1/sms/sender
    Then the HTTP status code is 401

  Scenario: Delete an SMS Sender
    Given a REST client
    And the create sender data loaded from password-sender
    And a sender exists for passwordRecovery
    And a token for the notificationService user
    When I make a DELETE request for /notification/v1/sms/sender/passwordRecovery
    Then the HTTP status code is 200

  Scenario: Create a new SMS Sender using JSON data
    Given a REST client
    And a token for the notificationService user
    And the data to create password-sender
    And a sender does not exists for passwordRecovery
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /notification/v1/sms/sender
    Then the HTTP status code is 201

  Scenario: Get an SMS Sender
    Given a REST client
    And a sender exists for passwordRecovery
    And a token for the notificationService user
    When I make a GET request for /notification/v1/sms/sender/passwordRecovery
    Then the HTTP status code is 200

