<?php

return [
    'resources' => [
        'notification' => [
            \MiamiOH\NotificationService\Resources\DefinitionsResourceProvider::class,
            \MiamiOH\NotificationService\Resources\EmailResourceProvider::class,
            \MiamiOH\NotificationService\Resources\ServicesResourceProvider::class,
            \MiamiOH\NotificationService\Resources\WasResourceProvider::class
        ]
    ]
];
