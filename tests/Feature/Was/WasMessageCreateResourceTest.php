<?php


namespace MiamiOH\NotificationService\Tests\Feature\Was;


use MiamiOH\RESTng\Connector\DataSource;

class WasMessageCreateResourceTest extends WasMessageTestBase
{
    public function testRequiresAuthorizationToCreateWasMessageForUser(): void
    {
        $response = $this->postJson('/notification/v1/was/message/my-app/doej');

        $response->assertStatus(401);
    }

    public function testCreatesMessageForUser(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $dataSource = DataSource::fromArray([
            'name' => 'test',
            'type' => 'Other',
            'host' => 'https://user.example.com',
            'user' => 'admin',
            'password' => 'passw0rd',
        ]);

        $this->dataSourceFactory->expects($this->exactly(2))->method('getDataSource')
            ->with($this->equalTo('notsrv_was_my-app'))
            ->willReturn($dataSource);

        $this->httpClient->expects($this->exactly(2))->method('get')
            ->willReturnOnConsecutiveCalls(
                '<xml><status>1</status></xml>',
                '<xml>
<record>
<uid>doej</uid>
<pidm>123456</pidm>
<complete>1</complete>
<reviewPending>0</reviewPending>
</record>
</xml>');

        $response = $this->postJson('/notification/v1/was/message/my-app/doej', ['complete' => 1, 'reviewPending' => 0]);

        $response->assertStatus(201);

        $messageData = $this->extractData($response);

        $expectedKeys = [
            'complete',
            'reviewPending',
        ];

        $this->assertEquals($expectedKeys, array_keys($messageData));
        $this->assertEquals(1, $messageData['complete']);
        $this->assertEquals(0, $messageData['reviewPending']);
    }
}