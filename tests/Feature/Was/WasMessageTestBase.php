<?php


namespace MiamiOH\NotificationService\Tests\Feature\Was;


use MiamiOH\NotificationService\Tests\Feature\FeatureTestCase;
use MiamiOH\NotificationService\Util\HttpClient;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use PHPUnit\Framework\MockObject\MockObject;

class WasMessageTestBase extends FeatureTestCase
{
    /** @var DataSourceFactory|MockObject  */
    protected $dataSourceFactory;

    /** @var HttpClient|MockObject  */
    protected $httpClient;

    public function setUp(): void
    {
        parent::setUp();

        $this->dataSourceFactory = $this->createMock(DataSourceFactory::class);

        $this->useService([
            'name' => 'APIDataSourceFactory',
            'object' => $this->dataSourceFactory,
            'description' => 'DataSource Factory',
        ]);

        $this->httpClient = $this->createMock(HttpClient::class);

        $this->useService([
            'name' => 'NotificationHTTPClient',
            'object' => $this->httpClient,
            'description' => 'Configuration',
        ]);
    }
}