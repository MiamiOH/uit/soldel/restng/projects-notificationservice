<?php


namespace MiamiOH\NotificationService\Tests\Feature\Was;


use MiamiOH\RESTng\Connector\DataSource;

class WasMessageGetResourceTest extends WasMessageTestBase
{
    public function testRequiresAuthorizationToGetWasMessageForUser(): void
    {
        $response = $this->getJson('/notification/v1/was/message/my-app/doej');

        $response->assertStatus(401);
    }

    public function testReturnsEmptyValuesWhenMessageForUserIsNotFound(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $dataSource = DataSource::fromArray([
            'name' => 'test',
            'type' => 'Other',
            'host' => 'https://user.example.com',
            'user' => 'admin',
            'password' => 'passw0rd',
        ]);

        $this->dataSourceFactory->expects($this->once())->method('getDataSource')
            ->with($this->equalTo('notsrv_was_my-app'))
            ->willReturn($dataSource);

        $this->httpClient->expects($this->once())->method('get')
            ->willReturn('<xml></xml>');

        $response = $this->getJson('/notification/v1/was/message/my-app/doej?daysPast=7&attributes=complete,reviewPending');

        $response->assertStatus(200);

        $messageData = $this->extractData($response);

        $expectedKeys = [
            'complete',
            'reviewPending',
        ];

        $this->assertEquals($expectedKeys, array_keys($messageData));
        $this->assertEmpty($messageData['complete']);
        $this->assertEmpty($messageData['reviewPending']);
    }

    public function testReturnsCorrectDataModelForAccount(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $dataSource = DataSource::fromArray([
            'name' => 'test',
            'type' => 'Other',
            'host' => 'https://user.example.com',
            'user' => 'admin',
            'password' => 'passw0rd',
        ]);

        $this->dataSourceFactory->expects($this->once())->method('getDataSource')
            ->with($this->equalTo('notsrv_was_my-app'))
            ->willReturn($dataSource);

        $this->httpClient->expects($this->once())->method('get')
            ->willReturn('<xml>
<record>
<uid>doej</uid>
<pidm>123456</pidm>
<complete>1</complete>
<reviewPending>0</reviewPending>
</record>
</xml>');

        $response = $this->getJson('/notification/v1/was/message/my-app/doej?daysPast=7&attributes=reviewPending,complete');

        $messageData = $this->extractData($response);

        $expectedKeys = [
            'reviewPending',
            'complete',
        ];

        $this->assertEquals($expectedKeys, array_keys($messageData));
        $this->assertEquals(1, $messageData['complete']);
        $this->assertEquals(0, $messageData['reviewPending']);
    }
}