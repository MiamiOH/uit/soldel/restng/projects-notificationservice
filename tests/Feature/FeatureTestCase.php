<?php


namespace MiamiOH\NotificationService\Tests\Feature;


use MiamiOH\NotificationService\Tests\TestCase;
use MiamiOH\NotificationService\Util\IdProvider;
use MiamiOH\RESTng\Testing\RefreshDatabase;
use MiamiOH\RESTng\Testing\TestResponse;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use MiamiOH\RESTng\Testing\UsesAuthorization;
use PHPUnit\Framework\MockObject\MockObject;

class FeatureTestCase extends TestCase
{
    use UsesAuthentication;
    use UsesAuthorization;
    use RefreshDatabase;

    protected $databaseConnections = ['NOTSRV_DB'];

    /** @var IdProvider|MockObject  */
    protected $idProvider;
    protected $nextId = 1;

    public function setUp(): void
    {
        parent::setUp();

        $this->idProvider = $this->createMock(IdProvider::class);
        $this->idProvider->method('nextEmailId')
            ->willReturnCallback(function () {
                $nextId = $this->nextId;
                $this->nextId++;
                return $nextId;
            });

        $this->useService([
            'name' => 'NotificationIdProvider',
            'object' => $this->idProvider,
            'description' => 'ID Provider',
        ]);
    }

    protected function extractData(TestResponse $response): array
    {
        $payload = json_decode($response->content(), true, 512, JSON_THROW_ON_ERROR);

        return $payload['data'];
    }
}
