<?php


namespace MiamiOH\NotificationService\Tests\Feature\Email;


use MiamiOH\NotificationService\Tests\Feature\FeatureTestCase;
use MiamiOH\RESTng\App;

class EmailCollectionGetResourceTest extends FeatureTestCase
{
    public function testRequiresAuthorizationToGetEmailCollection(): void
    {
        $response = $this->getJson('/notification/v1/email/collection');

        $response->assertStatus(401);
    }

    public function testReturnsNotImplemented(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->getJson('/notification/v1/email/collection');

        $response->assertStatus(App::API_NOTIMPLEMENTED);
    }
}