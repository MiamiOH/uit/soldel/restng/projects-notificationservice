<?php


namespace MiamiOH\NotificationService\Tests\Feature\Email;


use Carbon\Carbon;
use MiamiOH\NotificationService\Email\EmailModel;
use MiamiOH\NotificationService\Email\EmailRepository;
use MiamiOH\NotificationService\Tests\Feature\FeatureTestCase;

class EmailMessageUpdateResourceTest extends FeatureTestCase
{
    public function testRequiresAuthorizationToUpdateEmailMessage(): void
    {
        $response = $this->putJson('/notification/v1/email/message');

        $response->assertStatus(401);
    }

    public function testRequiresAuthorizationToUpdateEmailMessageById(): void
    {
        $response = $this->putJson('/notification/v1/email/message/123');

        $response->assertStatus(401);
    }

    public function testReturnsSuccessWhenUpdatingCollectionOfEmailMessages(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->putJson('/notification/v1/email/message');

        $response->assertStatus(200);
    }

    public function testReturnsSuccessForSentMessageWhenUpdatingCollection(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 222, 'status' => 'P']);

        $data = [
            ['id' => 222, 'status' => 'S'],
        ];

        $response = $this->putJson('/notification/v1/email/message', $data);

        $response->assertStatus(200);

        $results = json_decode($response->content(), true);

        $this->assertEquals(200, $results['data'][0]['status']);

        $model = EmailModel::where('id', '=', 222)->firstOrFail();
        $this->assertEquals('S', $model->status);
    }

    public function testProcessesAllMessagesInCollectionIfOneFails(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 222, 'status' => 'P']);
        $this->createEmail(['id' => 333, 'status' => 'P']);
        $this->createEmail(['id' => 444, 'status' => 'P']);

        $data = [
            ['id' => 222, 'status' => 'P'],
            ['id' => 333, 'status' => 'S'],
            ['id' => 444, 'status' => 'S'],
        ];

        $response = $this->putJson('/notification/v1/email/message', $data);

        $response->assertStatus(200);

        $results = json_decode($response->content(), true);

        $this->assertEquals(400, $results['data'][0]['status']);
        $this->assertEquals(200, $results['data'][1]['status']);
        $this->assertEquals(200, $results['data'][2]['status']);

        $model = EmailModel::where('id', '=', 222)->firstOrFail();
        $this->assertEquals('P', $model->status);
        $model = EmailModel::where('id', '=', 333)->firstOrFail();
        $this->assertEquals('S', $model->status);
        $model = EmailModel::where('id', '=', 444)->firstOrFail();
        $this->assertEquals('S', $model->status);
    }

    public function testReturnsNotFoundWhenEmailMessageIdIsNotFound(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->putJson('/notification/v1/email/message/123');

        $response->assertStatus(404);
    }

    public function testUpdatesStatusFromPendingToSentForEmailMessage(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 222, 'status' => 'P']);

        $response = $this->putJson('/notification/v1/email/message/222', ['status' => 'S']);

        $response->assertStatus(200);

        $model = EmailModel::where('id', '=', 222)->firstOrFail();
        $this->assertEquals('S', $model->status);
    }

    public function testUpdatesEndDateForEmailMessageWhenSent(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $expected = Carbon::create(2020, 5, 21, 9, 10, 32);
        Carbon::setTestNow($expected);

        $this->createEmail(['id' => 123, 'status' => 'P', 'start_date' => $expected->clone()->subHour()]);

        $response = $this->putJson('/notification/v1/email/message/123', ['status' => 'S']);

        $response->assertStatus(200);

        $model = EmailModel::where('id', '=', 123)->firstOrFail();
        $this->assertEquals($expected, $model->end_date);
    }

    public function testDoesNotUpdateEndDateForEmailMessageOnError(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $expected = Carbon::create(2020, 5, 21, 9, 10, 32);
        Carbon::setTestNow($expected);

        $this->createEmail(['id' => 123, 'status' => 'P']);

        $response = $this->putJson('/notification/v1/email/message/123', ['status' => 'E']);

        $response->assertStatus(200);

        $model = EmailModel::where('id', '=', 123)->firstOrFail();
        $this->assertEquals('E', $model->status);
        $this->assertNull($model->end_date);
    }

    public function testSetsErrorCountToOneForEmailMessageOnFirstError(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $expected = Carbon::create(2020, 5, 21, 9, 10, 32);
        Carbon::setTestNow($expected);

        $this->createEmail(['id' => 123, 'status' => 'P']);

        $response = $this->putJson('/notification/v1/email/message/123', ['status' => 'E']);

        $response->assertStatus(200);

        $model = EmailModel::where('id', '=', 123)->firstOrFail();
        $this->assertEquals('E', $model->status);
        $this->assertEquals(1, $model->error_count);
    }

    public function testIncrementsErrorCountForEmailMessageOnErrorStatus(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 123, 'status' => 'E', 'error_count' => 1]);

        $response = $this->putJson(
            '/notification/v1/email/message/123',
            ['status' => 'E', 'errorMessage' => 'Failed to send'],
        );

        $response->assertStatus(200);

        $model = EmailModel::where('id', '=', 123)->firstOrFail();
        $this->assertEquals('E', $model->status);
        $this->assertEquals(2, $model->error_count);
    }

    public function testUpdatesErrorMessageForEmailMessage(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 123, 'status' => 'P']);

        $response = $this->putJson(
            '/notification/v1/email/message/123',
            ['status' => 'E', 'errorMessage' => 'Failed to send'],
        );

        $response->assertStatus(200);

        $model = EmailModel::where('id', '=', 123)->firstOrFail();
        $this->assertEquals('E', $model->status);
        $this->assertEquals('Failed to send', $model->error_message);
    }

    public function testUsesDefaultErrorMessageForEmailMessageWhenNoErrorStringGiven(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 123, 'status' => 'P']);

        $response = $this->putJson('/notification/v1/email/message/123', ['status' => 'E'],);

        $response->assertStatus(200);

        $model = EmailModel::where('id', '=', 123)->firstOrFail();
        $this->assertEquals('E', $model->status);
        $this->assertEquals('Unknown error', $model->error_message);
    }

    public function testSetsScheduleDateToExpectedDelayForEmailMessageOnError(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $now = Carbon::create(2020, 5, 21, 9, 10, 32);
        Carbon::setTestNow($now);

        $expected = $now->clone()->addMinutes(5 * EmailRepository::DELAY_MINUTES_FACTOR);

        $this->createEmail(['id' => 123, 'status' => 'P', 'priority' => 5, 'scheduled_date' => $now]);

        $response = $this->putJson('/notification/v1/email/message/123', ['status' => 'E']);

        $response->assertStatus(200);

        $model = EmailModel::where('id', '=', 123)->firstOrFail();
        $this->assertEquals($expected, $model->scheduled_date);
    }

    public function testSetsToFailedWhenMaxErrorsIsExceededForEmailMessageOnError(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 123, 'status' => 'E', 'error_count' => EmailRepository::MAX_FAILURES - 1]);

        $response = $this->putJson('/notification/v1/email/message/123', ['status' => 'E']);

        $response->assertStatus(200);

        $model = EmailModel::where('id', '=', 123)->firstOrFail();
        $this->assertEquals('F', $model->status);
    }

    /**
     * @dataProvider lockedStatusChecks
     */
    public function testCannotUpdateEmailMessageThatIsSentOrFailed(string $status): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 123, 'status' => $status]);

        $response = $this->putJson('/notification/v1/email/message/123', ['status' => 'S']);

        $response->assertStatus(400);
    }

    public function lockedStatusChecks(): array
    {
        return [
            'sent' => ['S'],
            'failed' => ['F'],
        ];
    }
}