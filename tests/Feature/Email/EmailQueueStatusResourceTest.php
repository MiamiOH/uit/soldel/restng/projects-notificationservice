<?php


namespace MiamiOH\NotificationService\Tests\Feature\Email;


use Carbon\Carbon;
use MiamiOH\NotificationService\Email\EmailRepository;
use MiamiOH\NotificationService\Tests\Feature\FeatureTestCase;

class EmailQueueStatusResourceTest extends FeatureTestCase
{
    public function testGetsEmailQueueStatusWithoutAuthentication(): void
    {
        $response = $this->getJson('/notification/v1/email/queue/status');

        $response->assertStatus(200);
    }

    public function testRetunsExpectedDataStructure(): void
    {
        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertCount(8, $messageData);
        $this->assertArrayHasKey('total_pending', $messageData);
        $this->assertArrayHasKey('span_start', $messageData);
        $this->assertArrayHasKey('span_message_count', $messageData);
        $this->assertArrayHasKey('span_sent_count', $messageData);
        $this->assertArrayHasKey('span_error_count', $messageData);
        $this->assertArrayHasKey('oldest_pending_minutes', $messageData);
        $this->assertArrayHasKey('status', $messageData);
        $this->assertArrayHasKey('message', $messageData);
    }

    public function testCountsTotalPending(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->addMinutes(10)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(10)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(110)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals(3, $messageData['total_pending']);
    }

    public function testUsesDefaultSpanOfSixtyMinutes(): void
    {
        Carbon::setTestNow(Carbon::create(2022, 11, 11, 10, 10, 34));
        $now = Carbon::now()->toImmutable();

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals($now->subMinutes(60)->toDateTimeString(), $messageData['span_start']);
    }

    public function testUsesSpanMinutesGivenInQuery(): void
    {
        Carbon::setTestNow(Carbon::create(2022, 11, 11, 10, 10, 34));
        $now = Carbon::now()->toImmutable();

        $spanMinutes = 35;

        $response = $this->getJson('/notification/v1/email/queue/status?span_minutes=' . $spanMinutes);

        $messageData = $this->extractData($response);

        $this->assertEquals($now->subMinutes($spanMinutes)->toDateTimeString(), $messageData['span_start']);
    }

    public function testOnlyCountsMessagesWithinSpan(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->addMinutes(10)]);
        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->subMinutes(10)]);
        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->subMinutes(110)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->addMinutes(10)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(10)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(110)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals(2, $messageData['span_message_count']);
    }

    public function testOnlyCountsSentMessagesWithinSpan(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->addMinutes(10)]);
        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->subMinutes(10)]);
        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->subMinutes(110)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->addMinutes(10)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(10)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(110)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals(1, $messageData['span_sent_count']);
    }

    public function testOnlyCountsErrorMessagesWithinSpan(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->addMinutes(10)]);
        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->subMinutes(10)]);
        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->subMinutes(110)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->addMinutes(10)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(10)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(110)]);
        $this->createEmail(['status' => 'E', 'scheduled_date' => $now->addMinutes(10)]);
        $this->createEmail(['status' => 'E', 'scheduled_date' => $now->subMinutes(10)]);
        $this->createEmail(['status' => 'E', 'scheduled_date' => $now->subMinutes(110)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals(1, $messageData['span_error_count']);
    }

    public function testFindsOldestHighestPriorityPendingMessage(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['priority' => 1, 'status' => 'P', 'scheduled_date' => $now->subMinutes(10)]);
        $this->createEmail(['priority' => 5, 'status' => 'P', 'scheduled_date' => $now->subMinutes(11)]);
        $this->createEmail(['priority' => 1, 'status' => 'P', 'scheduled_date' => $now->subMinutes(20)]);
        $this->createEmail(['priority' => 5, 'status' => 'P', 'scheduled_date' => $now->subMinutes(30)]);
        $this->createEmail(['priority' => 1, 'status' => 'P', 'scheduled_date' => $now->subMinutes(90)]);
        $this->createEmail(['priority' => 5, 'status' => 'P', 'scheduled_date' => $now->subMinutes(100)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals(90, $messageData['oldest_pending_minutes']);
    }

    public function testStatusIsOkWithNoErrors(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(1)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(2)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(3)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals(EmailRepository::QUEUE_STATUS_OK, $messageData['status']);
    }

    public function testStatusIsCriticalIfSpanMessageErrorRateIsOneHundred(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'E', 'scheduled_date' => $now->subMinutes(1)]);
        $this->createEmail(['status' => 'E', 'scheduled_date' => $now->subMinutes(2)]);
        $this->createEmail(['status' => 'E', 'scheduled_date' => $now->subMinutes(3)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals(EmailRepository::QUEUE_STATUS_CRITICAL, $messageData['status']);
    }

    public function testStatusIsCriticalIfOldestMessageMinutesIsGreaterThanDefaultMinutes(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(1)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(2)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(65)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals(EmailRepository::QUEUE_STATUS_CRITICAL, $messageData['status']);
    }

    public function testStatusIsCriticalIfOldestMessageMinutesIsGreaterThanGivenMinutes(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(1)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(2)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(15)]);

        $response = $this->getJson('/notification/v1/email/queue/status?oldest_message_minutes=10');

        $messageData = $this->extractData($response);

        $this->assertEquals(EmailRepository::QUEUE_STATUS_CRITICAL, $messageData['status']);
    }

    public function testStatusIsWarningIfOldestMessageMinutesIsGreaterThanSpan(): void
    {
        $now = Carbon::now()->toImmutable();

        $spanMinutes = 10;

        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(1)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(2)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes($spanMinutes + 1)]);

        $response = $this->getJson('/notification/v1/email/queue/status?span_minutes=' . $spanMinutes);

        $messageData = $this->extractData($response);

        $this->assertEquals(EmailRepository::QUEUE_STATUS_WARNING, $messageData['status']);
    }

    public function testStatusIsWarningIfSpanMessageErrorCountIsGreaterThanZero(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(1)]);
        $this->createEmail(['status' => 'E', 'scheduled_date' => $now->subMinutes(2)]);
        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(3)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $messageData = $this->extractData($response);

        $this->assertEquals(EmailRepository::QUEUE_STATUS_WARNING, $messageData['status']);
    }

    public function testHttpResponseStatusIs200WhenWarning(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'S', 'scheduled_date' => $now->subMinutes(6)]);
        $this->createEmail(['status' => 'E', 'scheduled_date' => $now->subMinutes(5)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $response->assertStatus(200);
    }

    public function testHttpResponseStatusIs500WhenCritical(): void
    {
        $now = Carbon::now()->toImmutable();

        $this->createEmail(['status' => 'P', 'scheduled_date' => $now->subMinutes(65)]);

        $response = $this->getJson('/notification/v1/email/queue/status');

        $response->assertStatus(500);
    }
}
