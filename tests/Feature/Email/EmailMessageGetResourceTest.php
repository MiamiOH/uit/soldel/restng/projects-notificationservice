<?php


namespace MiamiOH\NotificationService\Tests\Feature\Email;


use Carbon\Carbon;
use MiamiOH\NotificationService\Tests\Feature\FeatureTestCase;
use MiamiOH\RESTng\Testing\TestResponse;

class EmailMessageGetResourceTest extends FeatureTestCase
{
    public function testRequiresAuthorizationToGetEmailMessageById(): void
    {
        $response = $this->getJson('/notification/v1/email/message/123');

        $response->assertStatus(401);
    }

    public function testReturnsNotFoundWhenEmailMessageIsNotFound(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->getJson('/notification/v1/email/message/123');

        $response->assertStatus(404);
    }

    public function testReturnsEmailMessage(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 123, 'subject' => 'Saying hello']);

        $response = $this->getJson('/notification/v1/email/message/123');

        $response->assertStatus(200);

        $messageData = $this->extractData($response);

        $this->assertEquals('Saying hello', $messageData['subject']);
    }


    public function testReturnsCorrectDataModelForEmailMessage(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $now = Carbon::now();
        $start = Carbon::now()->subHour();
        $scheduled = Carbon::now()->subHour();
        $end = Carbon::now()->addHour();

        $data = [
            'id' => 123,
            'priority' => 1,
            'start_date' => $start,
            'end_date' => $end,
            'scheduled_date' => $scheduled,
            'to_addr' => 'doej@example.com',
            'from_addr' => 'help@example.com',
            'subject' => 'Your request',
            'body' => 'We cannot help you. Sorry.',
        ];

        $this->createEmail($data);

        $response = $this->getJson('/notification/v1/email/message/123');

        $messageData = $this->extractData($response);

        $expectedKeys = [
            'id',
            'status',
            'priority',
            'startDate',
            'endDate',
            'scheduledDate',
            'toAddr',
            'fromAddr',
            'subject',
            'body',
        ];

        $this->assertEquals($expectedKeys, array_keys($messageData));

        $this->assertEquals($data['id'], $messageData['id']);
        $this->assertEquals('P', $messageData['status']);
        $this->assertEquals($data['priority'], $messageData['priority']);
        $this->assertEquals($start->toIso8601ZuluString(), $messageData['startDate']);
        $this->assertEquals($end->toIso8601ZuluString(), $messageData['endDate']);
        $this->assertEquals($scheduled->toIso8601ZuluString(), $messageData['scheduledDate']);
        $this->assertEquals($data['to_addr'], $messageData['toAddr']);
        $this->assertEquals($data['from_addr'], $messageData['fromAddr']);
        $this->assertEquals($data['subject'], $messageData['subject']);
        $this->assertEquals($data['body'], $messageData['body']);
    }
}