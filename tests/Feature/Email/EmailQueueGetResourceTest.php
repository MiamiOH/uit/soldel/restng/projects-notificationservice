<?php


namespace MiamiOH\NotificationService\Tests\Feature\Email;


use Carbon\Carbon;
use MiamiOH\NotificationService\Tests\Feature\FeatureTestCase;

class EmailQueueGetResourceTest extends FeatureTestCase
{
    public function testRequiresAuthorizationToGetEmailQueue(): void
    {
        $response = $this->getJson('/notification/v1/email/queue');

        $response->assertStatus(401);
    }

    public function testReturnsEmptyCollectionWhenNoMessagesArePending(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->getJson('/notification/v1/email/queue');

        $response->assertStatus(200);

        $payload = json_decode($response->content(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertEmpty($payload['data']);
    }

    public function testReturnsProperMessageModels(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $start = Carbon::now()->subHour();
        $scheduled = Carbon::now()->subHour();
        $end = Carbon::now()->addHour();

        $data = [
            'id' => 123,
            'priority' => 1,
            'start_date' => $start,
            'end_date' => $end,
            'scheduled_date' => $scheduled,
            'to_addr' => 'doej@example.com',
            'from_addr' => 'help@example.com',
            'subject' => 'Your request',
            'body' => 'We cannot help you. Sorry.',
        ];

        $this->createEmail($data);

        $response = $this->getJson('/notification/v1/email/queue');

        $response->assertStatus(200);

        $messageData = $this->extractData($response)[0];

        $expectedKeys = [
            'id',
            'status',
            'priority',
            'startDate',
            'endDate',
            'scheduledDate',
            'toAddr',
            'fromAddr',
            'subject',
            'body',
        ];

        $this->assertEquals($expectedKeys, array_keys($messageData));

        $this->assertEquals($data['id'], $messageData['id']);
        $this->assertEquals('P', $messageData['status']);
        $this->assertEquals($data['priority'], $messageData['priority']);
        $this->assertEquals($start->toIso8601ZuluString(), $messageData['startDate']);
        $this->assertEquals($end->toIso8601ZuluString(), $messageData['endDate']);
        $this->assertEquals($scheduled->toIso8601ZuluString(), $messageData['scheduledDate']);
        $this->assertEquals($data['to_addr'], $messageData['toAddr']);
        $this->assertEquals($data['from_addr'], $messageData['fromAddr']);
        $this->assertEquals($data['subject'], $messageData['subject']);
        $this->assertEquals($data['body'], $messageData['body']);
    }

    public function testReturnsOnlyActionableMessages(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 123, 'status' => 'P']);
        $this->createEmail(['id' => 124, 'status' => 'S']);
        $this->createEmail(['id' => 125, 'status' => 'E']);

        $response = $this->getJson('/notification/v1/email/queue');

        $response->assertStatus(200);

        $messageData = $this->extractData($response);

        $this->assertCount(2, $messageData);
    }

    public function testReturnsOnlyActionableMessagesScheduledBeforeCurrentTime(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 122, 'status' => 'P', 'scheduled_date' => Carbon::now()->addMinute()]);
        $this->createEmail(['id' => 123, 'status' => 'P', 'scheduled_date' => Carbon::now()->subMinute()]);
        $this->createEmail(['id' => 124, 'status' => 'S', 'scheduled_date' => Carbon::now()->subMinute()]);
        $this->createEmail(['id' => 125, 'status' => 'E', 'scheduled_date' => Carbon::now()->subMinute()]);

        $response = $this->getJson('/notification/v1/email/queue');

        $response->assertStatus(200);

        $messageData = $this->extractData($response);

        $this->assertCount(2, $messageData);
    }

    public function testReturnsMessagesOrderedByPriorityAndDate(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->createEmail(['id' => 122, 'status' => 'P', 'priority' => '1', 'scheduled_date' => Carbon::now()->subMinutes(1)]);
        $this->createEmail(['id' => 124, 'status' => 'P', 'priority' => '3', 'scheduled_date' => Carbon::now()->subMinutes(2)]);
        $this->createEmail(['id' => 125, 'status' => 'P', 'priority' => '5', 'scheduled_date' => Carbon::now()->subMinutes(5)]);
        $this->createEmail(['id' => 123, 'status' => 'P', 'priority' => '3', 'scheduled_date' => Carbon::now()->subMinutes(3)]);

        $response = $this->getJson('/notification/v1/email/queue');

        $response->assertStatus(200);

        $messageData = $this->extractData($response);

        $this->assertCount(4, $messageData);
        $this->assertEquals(122, $messageData[0]['id']);
        $this->assertEquals(123, $messageData[1]['id']);
        $this->assertEquals(124, $messageData[2]['id']);
        $this->assertEquals(125, $messageData[3]['id']);
    }

    public function testReturnsOnlyRequestedMaxMessages(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        for ($i=0; $i < 12; $i++) {
            $this->createEmail(['id' => 100 + $i, 'status' => 'P', 'scheduled_date' => Carbon::now()->subMinutes($i)]);
        }

        $response = $this->getJson('/notification/v1/email/queue?max=10');

        $response->assertStatus(200);

        $messageData = $this->extractData($response);

        $this->assertCount(10, $messageData);
    }

    public function testOnlySupportsMaxUpToOneThousand(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        for ($i=0; $i < 1; $i++) {
            $this->createEmail(['id' => 1000 + $i, 'status' => 'P', 'scheduled_date' => Carbon::now()->subMinutes($i)]);
        }

        $response = $this->getJson('/notification/v1/email/queue?max=1200');

        $response->assertStatus(400);
    }

    public function testReturnsDefaultMaxMessages(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        for ($i=0; $i < 1100; $i++) {
            $this->createEmail(['id' => 100 + $i, 'status' => 'P', 'scheduled_date' => Carbon::now()->subMinutes($i)]);
        }

        $response = $this->getJson('/notification/v1/email/queue');

        $response->assertStatus(200);

        $messageData = $this->extractData($response);

        $this->assertCount(1000, $messageData);
    }
}