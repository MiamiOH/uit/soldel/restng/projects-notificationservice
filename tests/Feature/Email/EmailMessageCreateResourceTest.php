<?php


namespace MiamiOH\NotificationService\Tests\Feature\Email;


use Carbon\Carbon;
use MiamiOH\NotificationService\Email\EmailModel;
use MiamiOH\NotificationService\Email\EmailService;
use MiamiOH\NotificationService\Tests\Feature\FeatureTestCase;
use MiamiOH\RESTng\Service;

class EmailMessageCreateResourceTest extends FeatureTestCase
{
    public function testRequiresAuthorizationToCreateEmailMessage(): void
    {
        $response = $this->postJson('/notification/v1/email/message');

        $response->assertStatus(401);
    }

    public function testReturnsNewDataWhenEmailMessageIsCreated(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $subject = 'Test message';

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(['subject' => $subject]),
                'dataType' => 'model',
            ]
        );

        $response->assertStatus(201);

        $messageData = $this->extractData($response);

        $model = EmailModel::where('subject', '=', $subject)->firstOrFail();

        $this->assertEquals($messageData['subject'], $model->subject);
    }

    public function testReturnsNewDataCollectionWhenMultipleEmailMessagesAreCreated(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $subjects = ['Test message 1', 'Test message 2'];

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => [
                    $this->makeEmailData(['subject' => $subjects[0]]),
                    $this->makeEmailData(['subject' => $subjects[1]]),
                ],
                'dataType' => 'collection',
            ]
        );

        $response->assertStatus(201);

        $messageData = $this->extractData($response);

        $model = EmailModel::where('subject', '=', $subjects[0])->firstOrFail();
        $this->assertEquals($messageData[0]['code'], 201);
        $this->assertEquals($messageData[0]['model']['subject'], $model->subject);

        $model = EmailModel::where('subject', '=', $subjects[1])->firstOrFail();
        $this->assertEquals($messageData[1]['code'], 201);
        $this->assertEquals($messageData[1]['model']['subject'], $model->subject);
    }

    public function testProcessesEntireCollectionWhenOneFailsToBeCreated(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $subjects = ['Test message 1', 'Test message 2'];

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => [
                    $this->makeEmailData(['subject' => $subjects[0], 'toAddr' => null]),
                    $this->makeEmailData(['subject' => $subjects[1]]),
                ],
                'dataType' => 'collection',
            ]
        );

        $response->assertStatus(201);

        $messageData = $this->extractData($response);

        $found = EmailModel::where('subject', '=', $subjects[0])->count();
        $this->assertEquals(0, $found);
        $this->assertEquals($messageData[0]['code'], 500);

        $model = EmailModel::where('subject', '=', $subjects[1])->firstOrFail();
        $this->assertEquals($messageData[1]['code'], 201);
        $this->assertEquals($messageData[1]['model']['subject'], $model->subject);
    }

    // test only allows known status codes [P, S, E, F]

    /**
     * @throws \JsonException
     * @dataProvider statusCodeChecks
     */
    public function testAllowsKnownStatusCodesWhenCreatingEmailMessage(string $status): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(['status' => $status]),
                'dataType' => 'model',
            ]
        );

        $messageData = $this->extractData($response);
        $this->assertEquals($status, $messageData['status']);
    }

    public function statusCodeChecks(): array
    {
        return [
            'pending' => ['P'],
            'sent' => ['S'],
            'error' => ['E'],
            'failed' => ['F'],
        ];
    }

    public function testReturnsBadRequestForUnknownStatus(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(['status' => 'X']),
                'dataType' => 'model',
            ]
        );

        $response->assertStatus(400);
    }

    public function testReturnsBadRequestForNonIntegerPriority(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(['priority' => 'X']),
                'dataType' => 'model',
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @param $priority
     * @param $expected
     * @throws \JsonException
     *
     * @dataProvider priorityValueChecks
     */
    public function testAcceptsValidPriorities($priority, $expected): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(['priority' => $priority]),
                'dataType' => 'model',
            ]
        );

        $response->assertStatus(201);
        $messageData = $this->extractData($response);

        $this->assertEquals($expected, $messageData['priority']);
    }

    public function priorityValueChecks(): array
    {
        return [
            'string priority' => ['1', 1],
            'int priority' => [1, 1],
            'decimal string priority' => ['1.1', 1],
        ];
    }

    public function testReturnsBadRequestForEmptyToAddress(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(['toAddr' => null]),
                'dataType' => 'model',
            ]
        );

        $response->assertStatus(400);
    }

    public function testReturnsBadRequestForInvalidToAddress(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(['toAddr' => 'bob']),
                'dataType' => 'model',
            ]
        );

        $response->assertStatus(400);
    }

    public function testReturnsBadRequestForInvalidFromAddress(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(['fromAddr' => 'bob']),
                'dataType' => 'model',
            ]
        );

        $response->assertStatus(400);
    }

    public function testUsesDefaultStatusForNewEmailMessage(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(),
                'dataType' => 'model',
            ]
        );

        $messageData = $this->extractData($response);
        $this->assertEquals('P', $messageData['status']);
    }

    public function testUsesDefaultPriorityForNewEmailMessage(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(),
                'dataType' => 'model',
            ]
        );

        $messageData = $this->extractData($response);
        $this->assertEquals(5, $messageData['priority']);
    }

    public function testUsesIncrementedIdForNewEmailMessage(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $this->nextId = 123;

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(),
                'dataType' => 'model',
            ]
        );

        $messageData = $this->extractData($response);
        $this->assertEquals(123, $messageData['id']);

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(),
                'dataType' => 'model',
            ]
        );

        $messageData = $this->extractData($response);
        $this->assertEquals(124, $messageData['id']);
    }

    public function testUsesCurrentDateForStartDateOfNewEmailMessage(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $expected = Carbon::create(2020, 5, 21, 9, 10, 32);
        Carbon::setTestNow($expected);

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(),
                'dataType' => 'model',
            ]
        );

        $messageData = $this->extractData($response);
        $this->assertEquals($expected->toIso8601ZuluString(), $messageData['startDate']);
    }

    public function testUsesCurrentDateForScheduledDateOfNewEmailMessage(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $expected = Carbon::create(2020, 5, 21, 9, 10, 32);
        Carbon::setTestNow($expected);

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(),
                'dataType' => 'model',
            ]
        );

        $messageData = $this->extractData($response);
        $this->assertEquals($expected->toIso8601ZuluString(), $messageData['scheduledDate']);
    }

    public function testRejectsMessageWhenBodyExceedsMaxSize(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        putenv('NOTIFICATION_SERVICE_EMAIL_MAX_BODY_SIZE_BYTES=10');

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => $this->makeEmailData(),
                'dataType' => 'model',
            ]
        );

        $response->assertStatus(400);

        putenv('NOTIFICATION_SERVICE_EMAIL_MAX_BODY_SIZE_BYTES');
    }

    public function testRejectsMessageThatExceedsSizeInCollection(): void
    {
        $this->withToken('abc123')->willAuthenticateUser();
        $this->willAuthorizeUser();

        $subjects = ['Test message 1', 'Test message 2'];

        putenv('NOTIFICATION_SERVICE_EMAIL_MAX_BODY_SIZE_BYTES=10');

        $response = $this->postJson(
            '/notification/v1/email/message',
            [
                'data' => [
                    $this->makeEmailData(['subject' => $subjects[0], 'body' => 'Hello']),
                    $this->makeEmailData(['subject' => $subjects[1]]),
                ],
                'dataType' => 'collection',
            ]
        );

        $response->assertStatus(201);

        $messageData = $this->extractData($response);

        $model = EmailModel::where('subject', '=', $subjects[0])->firstOrFail();
        $this->assertEquals($messageData[0]['code'], 201);
        $this->assertEquals($messageData[0]['model']['subject'], $model->subject);

        $this->assertEquals($messageData[1]['code'], 400);

        putenv('NOTIFICATION_SERVICE_EMAIL_MAX_BODY_SIZE_BYTES');
    }

    private function makeEmailData(array $overrides = []): array
    {
        return array_merge([
            'toAddr' => 'publicjq@example.com',
            'fromAddr' => 'help@example.com',
            'subject' => 'Update on your issue',
            'body' => 'This is an update on your reported problem.',
        ], $overrides);
    }
}
