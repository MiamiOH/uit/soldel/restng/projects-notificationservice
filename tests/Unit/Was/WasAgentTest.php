<?php

namespace MiamiOH\NotificationService\Tests\Unit\Was;

use MiamiOH\NotificationService\Exception\WasNotificationException;
use MiamiOH\NotificationService\Util\HttpClient;
use MiamiOH\NotificationService\Was\WasAgent;
use MiamiOH\RESTng\Connector\DataSource;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class WasAgentTest extends TestCase
{
    /** @var WasAgent */
    private $agent;

    /** @var DataSourceFactory|MockObject  */
    private $dataSourceFactory;
    /** @var HttpClient|MockObject  */
    private $httpClient;

    public function setUp(): void
    {
        parent::setUp();

        $this->dataSourceFactory = $this->createMock(DataSourceFactory::class);
        $this->httpClient = $this->createMock(HttpClient::class);

        $this->agent = new WasAgent();

        $this->agent->setDataSourceFactory($this->dataSourceFactory);
        $this->agent->setHttpClient($this->httpClient);
    }

    public function testReturnsEmptyValuesWhenNoMessageIsFoundForUser(): void
    {
        $appKey = 'my-app';
        $uid = 'doej';

        $options = [
            'daysPast' => 10,
            'attributes' => ['completed', 'required'],
        ];

        $this->useDataSource('notsrv_was_my-app');

        $this->httpClient->expects($this->once())->method('get')
            ->willReturn('<xml></xml>');

        $data = $this->agent->getAppMessageForUser($appKey, $uid, $options);

        $this->assertArrayHasKey('completed', $data);
        $this->assertArrayHasKey('required', $data);
        $this->assertEmpty($data['completed']);
        $this->assertEmpty($data['required']);
    }

    public function testReturnsValuesWhenMessageIsFoundForUser(): void
    {
        $appKey = 'my-app';
        $uid = 'doej';

        $options = [
            'daysPast' => 10,
            'attributes' => ['completed', 'required'],
        ];

        $this->useDataSource('notsrv_was_my-app');

        $this->httpClient->expects($this->once())->method('get')
            ->willReturn('<xml>
<record>
<uid>doej</uid>
<pidm>123456</pidm>
<completed>1</completed>
<required>0</required>
</record>
</xml>');

        $data = $this->agent->getAppMessageForUser($appKey, $uid, $options);

        $this->assertArrayHasKey('completed', $data);
        $this->assertArrayHasKey('required', $data);
        $this->assertEquals(1, $data['completed']);
        $this->assertEquals(0, $data['required']);
    }

    public function testCreatesMessageForUser(): void
    {
        $appKey = 'my-app';
        $uid = 'doej';

        $attributes = [
            'completed' => 1,
            'required' => 0,
        ];

        $this->useDataSource('notsrv_was_my-app', ['user' => 'my-app']);

        $this->httpClient->expects($this->once())->method('get')
            ->with($this->callback(function (string $url) {
                $this->assertEquals(
                    'https://user.example.com/extapplog?extapp=my-app&ss=passw0rd&uid=doej&completed=1&required=0',
                    $url
                );
                return true;
            }))
            ->willReturn('<xml><status>1</status></xml>');

        $this->agent->createAppMessageForUser($appKey, $uid, $attributes);
    }

    public function testThrowsExcaptionIfMessageIsNotCreatedForUser(): void
    {
        $appKey = 'my-app';
        $uid = 'doej';

        $attributes = [
            'completed' => 1,
            'required' => 0,
        ];

        $this->useDataSource('notsrv_was_my-app');

        $this->httpClient->expects($this->once())->method('get')
            ->willReturn('<xml><status>0</status></xml>');

        $this->expectException(WasNotificationException::class);

        $this->agent->createAppMessageForUser($appKey, $uid, $attributes);
    }

    private function useDataSource(string $name, array $overrides = []): void
    {
        $attributes = array_merge([
            'name' => 'test',
            'type' => 'Other',
            'host' => 'https://user.example.com',
            'user' => 'admin',
            'password' => 'passw0rd',
        ], $overrides);

        $dataSource = DataSource::fromArray($attributes);

        $this->dataSourceFactory->expects($this->once())->method('getDataSource')
            ->with($this->equalTo($name))
            ->willReturn($dataSource);
    }
}
