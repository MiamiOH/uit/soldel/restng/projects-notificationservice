<?php

namespace MiamiOH\NotificationService\Tests;

use Carbon\Carbon;
use MiamiOH\NotificationService\Email\Email;
use MiamiOH\NotificationService\Email\EmailModel;
use PHPUnit\Framework\MockObject\MockObject;

abstract class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    protected const SUCCEED_ON_SEND = false;
    protected const FAIL_ON_SEND = true;
    protected const SECONDS_DELAY = 60;
    protected const MAX_TOTAL_ATTEMPTS = 10;

    protected $senderId = 111;
    protected $senderDelaySeconds = 90;

    /** @var ProviderFactory|MockObject  */
    protected $providerFactory;

    /** @var Provider|MockObject */
    protected $provider;

    protected function makeEmail(array $overrides = []): Email
    {
        $attributes = array_merge([
            'id' => random_int(1, 9999),
            'status' => 'P',
            'priority' => 3,
            'error_message' => '',
            'error_count' => 0,
            'uuid' => 'uid',
            'start_date' => Carbon::now(),
            'end_date' => null,
            'scheduled_date' => Carbon::now(),
            'to_addr' => 'doej@example.com',
            'from_addr' => 'help@example.com',
            'subject' => 'Your request',
            'body' => 'We cannot help you. Sorry.',
            'activity_date' => Carbon::now(),
        ], $overrides);

        return new EmailModel($attributes);
    }

    protected function createEmail(array $overrides = []): Email
    {
        $email = $this->makeEmail($overrides);

        $email->save();

        return $email;
    }
}
