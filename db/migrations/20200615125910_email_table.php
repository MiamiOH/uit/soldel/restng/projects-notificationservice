<?php

use Phinx\Migration\AbstractMigration;

class EmailTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('notsrv_email', ['id' => false])
            ->addColumn('id', 'integer')
            ->addColumn('status', 'string', ['limit' => 2, 'null' => true])
            ->addColumn('priority', 'integer')
            ->addColumn('error_message', 'string', ['limit' => 1024, 'null' => true])
            ->addColumn('error_count', 'integer', ['null' => true])
            ->addColumn('uuid', 'string', ['limit' => 256, 'null' => true])
            ->addColumn('start_date', 'datetime')
            ->addColumn('end_date', 'datetime', ['null' => true])
            ->addColumn('scheduled_date', 'datetime')
            ->addColumn('activity_date', 'datetime')
            ->addColumn('to_addr', 'string', ['limit' => 256])
            ->addColumn('from_addr', 'string', ['limit' => 256, 'null' => true])
            ->addColumn('subject', 'string', ['limit' => 256, 'null' => true])
            ->addColumn('body', 'text', ['null' => true])
            ->addIndex(['id'], ['unique' => true])
            ->addIndex(['status'])
            ->addIndex(['priority'])
            ->addIndex(['uuid'])
            ->create();
    }
}
