<?php

use Phinx\Migration\AbstractMigration;

class SmsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('notsrv_sms', ['id' => false])
            ->addColumn('id', 'integer')
            ->addColumn('phone_number', 'string', ['limit' => 25])
            ->addColumn('sender_id', 'integer')
            ->addColumn('body', 'string', ['limit' => 255])
            ->addColumn('priority', 'integer')
            ->addColumn('error_message', 'string', ['limit' => 1024, 'null' => true])
            ->addColumn('error_count', 'integer', ['null' => true])
            ->addColumn('end_date', 'datetime', ['null' => true])
            ->addColumn('scheduled_date', 'datetime')
            ->addColumn('uuid', 'string', ['limit' => 256, 'null' => true])
            ->addColumn('status', 'string', ['limit' => 10, 'null' => true])
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
            ->addIndex(['id'], ['unique' => true])
            ->addForeignKey(['sender_id'], 'notsrv_sender', 'id')
            ->create();
    }
}
