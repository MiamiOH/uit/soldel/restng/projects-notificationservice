<?php

use Phinx\Migration\AbstractMigration;

class SenderAccountTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('notsrv_sendr_acct', ['id' => false])
            ->addColumn('id', 'integer')
            ->addColumn('ordinal', 'integer')
            ->addColumn('from_number', 'string', ['limit' => 25])
            ->addColumn('sender_id', 'integer')
            ->addColumn('account_id', 'integer')
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
            ->addIndex(['id'], ['unique' => true])
            ->addForeignKey(['sender_id'], 'notsrv_sender', 'id')
            ->addForeignKey(['account_id'], 'notsrv_accountr', 'id')
            ->create();
    }
}
