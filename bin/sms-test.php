<?php

use MiamiOH\NotificationService\Sms\SmsProvider\NexmoProvider;
use MiamiOH\NotificationService\Sms\SmsProvider\Provider;
use MiamiOH\NotificationService\Sms\SmsProvider\TwilioProvider;

require 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::create(__DIR__ . DIRECTORY_SEPARATOR . '..');
$dotenv->load();

if (empty($argv[1])) {
    throw new InvalidArgumentException('You must provide a recipient phone number for testing');
}

if (strpos($argv[1], 'help') !== false) {
    print "Usage: php bin/sms-test.php +15551234567 [Twilio|Nexmo]\n";
    exit;
}

$providerName = $argv[2] ?? 'Twilio';

$provider = makeProvider($providerName);

$provider->send(getenv('TO_NUMBER'), 'This is a test message');

function makeProvider(string $providerName): Provider
{
    if ($providerName === 'Nexmo') {
        return new NexmoProvider(
            getenv('NEXMO_USERNAME'),
            getenv('NEXMO_PASSWORD'),
            getenv('NEXMO_FROM_NUMBER')
        );
    }

    if ($providerName === 'Twilio') {
        return new TwilioProvider(
            getenv('TWILIO_USERNAME'),
            getenv('TWILIO_PASSWORD'),
            getenv('TWILIO_FROM_NUMBER')
        );
    }

    throw new InvalidArgumentException(sprintf('Invalid SMS Provider "%s"', $providerName));
}