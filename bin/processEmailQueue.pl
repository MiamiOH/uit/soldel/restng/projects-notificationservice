#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

use LWP::UserAgent;
use JSON;
use Try::Tiny;
use Email::Stuffer;
use Email::Sender::Transport::SMTP;
use Data::Dumper;

# Check to make sure there is not already a instance running.
my $programName = $0;
$programName =~ s/.*\/(.*)$/$1/;
my $pid = `ps -C $programName -o pid=`;
chomp $pid;

if (grep { $_ != $$ } split("\n", $pid)) {
  # Print a notice, but...
  print STDERR "$programName is already running\n";
  
  # this is not an error condition.
  exit 0;
}

# 1. Declare our configuration variables, but do not define them. We
#    will do a defined() test later.
my $logPath;
my $logFileName;
my $keepLogs;
my $wsServer;
my $smtpServer;
my $queueSize;
my $saveBatch;
my $overrideToAddr;
my $addressFilter;
my $batchSize;
my $batchDelay;

my $configFile = '';
our $VERBOSE = 0;
my $help = 0;

# 2. Read any options provided on the command line into our configuration
#    variables. This will cause those provided to be defined.
my $result = GetOptions(
  'log_dir=s' => \$logPath,
  'log=s' => \$logFileName, 
  'log_history=s' => \$keepLogs,
  'ws_server=s' => \$wsServer, 
  'smtp_server=s' => \$smtpServer,
  'queue_size=s' => \$queueSize,
  'save_batch=s' => \$saveBatch,
  'to_addr=s' => \$overrideToAddr,
  'filter=s' => \$addressFilter,
  'batch_size=s' => \$batchSize,
  'batch_delay=s' => \$batchDelay,
  'config=s' => \$configFile, 
  'verbose' => \$VERBOSE, 
  'help' => \$help
  );

# 3. Document the command line options (and by extension the config file).
if ($help) {
  print <<EOP;

Process a queue of email messages

Options:

     log_dir = path to log file
         log = log file name
 log_history = number of log files to keep [10]
   ws_server = URL to web service
 smtp_server = DNS name or IP address of smtp server
  queue_size = number of entries to request from the queue
  save_batch = number of messages to send before saving
     to_addr = SMTP address to override incoming for testing
      filter = RegEx white list for email addresses
  batch_size = number of messages per batch_size
 batch_delay = delay in seconds between batches
      config = path to config file
     verbose = verbose output
        help = display this help

EOP
}

# 4. Read the config file if provided.
our %config;
if ($configFile) {
  open CONFIG, $configFile or die "Couldn't open $configFile: $!";
  while (<CONFIG>) {
    chomp;
    next unless ($_);
    next if (/^#/);
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
  }
  close CONFIG;
}

# 5. Set up configuration from the config file, allowing override by
#    command line options if provided.
$logPath = $config{'log_dir'}
  if (!defined($logPath) && exists($config{'log_dir'}));
$logFileName = $config{'log'}
  if (!defined($logFileName) && exists($config{'log'}));
$keepLogs = $config{'log_history'}
  if (!defined($keepLogs) && exists($config{'log_history'}));
$wsServer = $config{'ws_server'}
  if (!defined($wsServer) && exists($config{'ws_server'}));
$smtpServer = $config{'smtp_server'}
  if (!defined($smtpServer) && exists($config{'smtp_server'}));
$queueSize = $config{'queue_size'}
  if (!defined($queueSize) && exists($config{'queue_size'}));
$saveBatch = $config{'save_batch'}
  if (!defined($saveBatch) && exists($config{'save_batch'}));
$overrideToAddr = $config{'to_addr'}
  if (!defined($overrideToAddr) && exists($config{'to_addr'}));
$addressFilter = $config{'filter'}
  if (!defined($addressFilter) && exists($config{'filter'}));
$batchSize = $config{'batch_size'}
  if (!defined($batchSize) && exists($config{'batch_size'}));
$batchDelay = $config{'batch_delay'}
  if (!defined($batchDelay) && exists($config{'batch_delay'}));

# 6. Apply default configuration for any missing items.
$logPath = '/var/logs/extract' unless (defined($logPath));
$logFileName = 'extract_log.txt' unless (defined($logFileName));
$keepLogs = 0 unless (defined($keepLogs));
$queueSize = 10 unless (defined($queueSize));
$saveBatch = 1 unless (defined($saveBatch));

unless ($wsServer) {
  print "No valid server provided in command line or config file\n";
  exit 1;
}
our $logFile = $logPath . '/' . $logFileName;

our $token = '';
our $tokenLastUpdate = 0;

checkMUAuthenticationToken();

my $ua = LWP::UserAgent->new();

my $lastBatchCount = 0;

print "filter: $addressFilter\n" if ($addressFilter && $VERBOSE);

do {
  my $request = HTTP::Request->new('GET', $wsServer . '/notification/v1/email/queue?' .
    'max=' . $queueSize . '&token=' . $token);
  my $response = $ua->simple_request($request);

  my $queue = extractResponseData('response' => $response, 'request' => $request);

  # We will loop until the last batch count is 0.
  $lastBatchCount = scalar(@{$queue});
  print "queue $lastBatchCount\n" if ($VERBOSE);

  my $count = 0;
  my $updates = [];
  while ( my $notification = shift @{$queue}) {

    if ($addressFilter) {
      unless ($notification->{'toAddr'} =~ /$addressFilter/i) {
        print "Skipping $notification->{'toAddr'}\n" if ($VERBOSE);
        next;
      }
    }

    # send email here
    my $email = new Email::Stuffer();
    
    # Use the smpt server if provided, otherwise it will default to sendmail
    if ($smtpServer) {
      print "Use smtp server $smtpServer\n" if ($VERBOSE);
      $email->transport('SMTP', { 'host' => $smtpServer });
    } else {
      print "use sendmail\n" if ($VERBOSE);
    }

    $email->from($notification->{'fromAddr'});
    $email->to($overrideToAddr ? $overrideToAddr : $notification->{'toAddr'});
    $email->subject($notification->{'subject'});
    $email->text_body($notification->{'body'});
    
    my($sent, $error, $trace);
    try {
      my $sendResult = $email->send_or_die() || '';
      $sent = $sendResult ? 1 : 0;
    } catch {
      ($error, $trace) = split("\n", $_, 2);
      logEntry('message' => 'To ' . $notification->{'toAddr'} . ': ' . "$error\n\n$trace");
    };

    push(@{$updates}, {
          'id' => $notification->{'id'},
          'status' => $error ? 'E' : 'S',
          'toAddr' => $notification->{'toAddr'},
          'error_message' => $error,
        });

    $count++;

    # Save if we are at our save point or the queue is empty
    if ($count % $saveBatch == 0 || scalar(@{$queue}) == 0) {
      
      checkMUAuthenticationToken();

      print "saving " . scalar(@{$updates}) . "\n" if ($VERBOSE);

      $request = HTTP::Request->new('PUT', $wsServer . '/notification/v1/email/message' .
         '?token=' . $token);

      my $body = $updates;

      $request->content(to_json($body));
      $request->header('Content-Type', 'application/json');

      $response = $ua->simple_request($request);

      unless ($response->code() == 200) {
        logEntry('message' => 'Failed to update notification status');
        logEntry('message' => $request->as_string());
        logEntry('message' => $response->as_string());
      }

      $updates = [];
    }

    # Pause for rate limiting at our batch size, but only if there are remaining items
    if ($count % $batchSize == 0 && scalar(@{$queue}) > 0) {
      print "Sent $count, time to sleep for $batchDelay seconds\n" if ($VERBOSE);
      sleep $batchDelay;
    }
    
  }
} while ($lastBatchCount);

sub extractResponseData {
  my $params = { @_ };

  my $request = $params->{'request'};
  my $response = $params->{'response'};
  my $exitOnError = defined($params->{'exitOnError'}) ? $params->{'exitOnError'} : 1;

  unless ($response->code == 200) {
    my $error = 'Request for service failed';

    $error = 'Authentication failed' if ($response->code == 401);

    if ($exitOnError) {
      exitWithError('message' => $error, 'request' => $request, 'response' => $response);
    } else {
      logEntry('message' => $error, 'request' => $request, 'response' => $response);
    }
  }

  my $payload = {};

  eval(q#$payload = from_json(${$response->content_ref()})#);

  if ($@) {
    exitWithError('message' => 'Failed to parse response ' . $@, 'request' => $request, 'response' => $response);
  }

  unless (defined($payload->{'data'})) {
    exitWithError('message' => 'Profile response contains no data element', 'request' => $request, 'response' => $response);
  }

  return $payload->{'data'};
}

sub exitWithError {
  my $params = { @_ };

  my $message = $params->{'message'} || 'An unknown error occurred';

  print "$message\n" if ($VERBOSE);
  print $params->{'request'}->as_string() if ($params->{'request'} && $VERBOSE);
  print $params->{'response'}->as_string() if ($params->{'response'} && $VERBOSE);
  logEntry('message' => $message);
  logEntry('message' => $params->{'request'}->as_string()) if ($params->{'request'});
  logEntry('message' => $params->{'response'}->as_string()) if ($params->{'response'});
  exit 1;
}

sub logEntry {
  my $params = { @_ };

  my $message = $params->{'message'} || '';

  if ($message) {
    my $time = scalar localtime();
    print "[$time] $message\n" if ($VERBOSE);
    open LOG, ">>$logFile" or die "Couldn't open $logFile: $!";
    print LOG "[$time] $message\n";
    close LOG;
  }
}

sub checkMUAuthenticationToken {
  if ($token && time() < $tokenLastUpdate + (55 * 60)) {
    return 1;
  }

  getMUAuthenticationToken();
}

sub getMUAuthenticationToken {
  my $ua = LWP::UserAgent->new();

  my $request = HTTP::Request->new('POST', $wsServer . '/authentication/v1');
  my $data = {
      'username' => $config{'username'},
      'password' => $config{'password'},
      'type' => 'usernamePassword'
  };

  $request->content(to_json($data));
  $request->header('Content-Type', 'application/json');

  my $response = $ua->simple_request($request);

  # Do not log the request since it has username/password
  my $tokenInfo = extractResponseData('response' => $response);

  $token = $tokenInfo->{'token'};
  $tokenLastUpdate = time();

  print "Got new token ($token)\n" if ($VERBOSE);
}

