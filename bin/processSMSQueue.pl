#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

use LWP::UserAgent;
use JSON;
use Try::Tiny;
use Data::Dumper;

# Check to make sure there is not already a instance running.
my $programName = $0;
$programName =~ s/.*\/(.*)$/$1/;
my $pid = `ps -C $programName -o pid=`;
chomp $pid;

if (grep { $_ != $$ } split("\n", $pid)) {
  # Print a notice, but...
  print STDERR "$programName is already running\n";
  
  # this is not an error condition.
  exit 0;
}

# 1. Declare our configuration variables, but do not define them. We
#    will do a defined() test later.
my $logPath;
my $logFileName;
my $keepLogs;
my $wsServer;
my $queueSize;
my $retries;
my $retriesDelay;

my $configFile = '';
our $VERBOSE = 0;
my $help = 0;

# 2. Read any options provided on the command line into our configuration
#    variables. This will cause those provided to be defined.
my $result = GetOptions(
  'log_dir=s' => \$logPath,
  'log=s' => \$logFileName, 
  'log_history=s' => \$keepLogs,
  'ws_server=s' => \$wsServer, 
  'queue_size=s' => \$queueSize,
  'retries=s' => \$retries,
  'retry_delay=s' => \$retriesDelay,
  'config=s' => \$configFile, 
  'verbose' => \$VERBOSE, 
  'help' => \$help
  );

# 3. Document the command line options (and by extension the config file).
if ($help) {
  print <<EOP;

Process a queue of SMS messages

Options:

     log_dir = path to log file
         log = log file name
 log_history = number of log files to keep [10]
   ws_server = URL to web service
  queue_size = number of entries to request from the queue
     retries = number times to retry after an empty queue
 retry_delay = delay in seconds between retries
      config = path to config file
     verbose = verbose output
        help = display this help

EOP
}

# 4. Read the config file if provided.
our %config;
if ($configFile) {
  open CONFIG, $configFile or die "Couldn't open $configFile: $!";
  while (<CONFIG>) {
    chomp;
    next unless ($_);
    next if (/^#/);
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
  }
  close CONFIG;
}

# 5. Set up configuration from the config file, allowing override by
#    command line options if provided.
$logPath = $config{'log_dir'}
  if (!defined($logPath) && exists($config{'log_dir'}));
$logFileName = $config{'log'}
  if (!defined($logFileName) && exists($config{'log'}));
$keepLogs = $config{'log_history'}
  if (!defined($keepLogs) && exists($config{'log_history'}));
$wsServer = $config{'ws_server'}
  if (!defined($wsServer) && exists($config{'ws_server'}));
$queueSize = $config{'queue_size'}
  if (!defined($queueSize) && exists($config{'queue_size'}));
$retries = $config{'retries'}
  if (!defined($retries) && exists($config{'retries'}));
$retriesDelay = $config{'retry_delay'}
  if (!defined($retriesDelay) && exists($config{'retry_delay'}));

# 6. Apply default configuration for any missing items.
$logPath = '/var/logs/extract' unless (defined($logPath));
$logFileName = 'extract_log.txt' unless (defined($logFileName));
$keepLogs = 0 unless (defined($keepLogs));
$queueSize = 10 unless (defined($queueSize));
$retries = 5 unless (defined($retries));
$retriesDelay = 30 unless (defined($retriesDelay));

unless ($wsServer) {
  print "No valid server provided in command line or config file\n";
  exit 1;
}
our $logFile = $logPath . '/' . $logFileName;

our $token = '';
our $tokenLastUpdate = 0;

checkMUAuthenticationToken();

my $ua = LWP::UserAgent->new();

my $lastBatchCount = 0;
my $retriesRemaining = $retries;

do {
  my $request = HTTP::Request->new('GET', $wsServer . '/notification/v1/sms/queue?' .
    'max=' . $queueSize . '&token=' . $token);
  my $response = $ua->simple_request($request);

  my $queue = extractResponseData('response' => $response, 'request' => $request);

  # We will loop until the last batch count is 0.
  $lastBatchCount = scalar(@{$queue});
  print "queue $lastBatchCount\n" if ($VERBOSE);

  my $count = 0;
  my $updates = [];
  while ( my $notification = shift @{$queue}) {
    # Call SMS sending resource with ids of messages to send.
    $request = HTTP::Request->new('PUT', $wsServer . '/notification/v1/sms/message/' .
    $notification->{'Id'} . '?token=' . $token);
    $response = $ua->simple_request($request);

    my $statusUpdate = extractResponseData('response' => $response, 'request' => $request);

    #print Dumper($statusUpdate);

    $count++;

  }

  # If there is nothing left sleep for 30 seconds and try again
  if ($lastBatchCount) {
    # Reset the remaining retries
    $retriesRemaining = $retries;
    print "$lastBatchCount entries, reset retries to $retriesRemaining\n" if ($VERBOSE);
  } else {
    $retriesRemaining--;
    print "empty last batch, sleeping $retriesDelay seconds, remaining retries $retriesRemaining\n" if ($VERBOSE);
    sleep $retriesDelay;
  }

} while ($lastBatchCount || $retriesRemaining);

sub extractResponseData {
  my $params = { @_ };

  my $request = $params->{'request'};
  my $response = $params->{'response'};
  my $exitOnError = defined($params->{'exitOnError'}) ? $params->{'exitOnError'} : 1;

  unless ($response->code == 200 || $response->code == 201) {
    my $error = 'Request for service failed';

    $error = 'Authentication failed' if ($response->code == 401);

    if ($exitOnError) {
      exitWithError('message' => $error, 'request' => $request, 'response' => $response);
    } else {
      logEntry('message' => $error, 'request' => $request, 'response' => $response);
    }
  }

  my $payload = {};

  eval(q#$payload = from_json(${$response->content_ref()})#);

  if ($@) {
    exitWithError('message' => 'Failed to parse response ' . $@, 'request' => $request, 'response' => $response);
  }

  unless (defined($payload->{'data'})) {
    exitWithError('message' => 'Profile response contains no data element', 'request' => $request, 'response' => $response);
  }

  return $payload->{'data'};
}

sub exitWithError {
  my $params = { @_ };

  my $message = $params->{'message'} || 'An unknown error occurred';

  print "$message\n" if ($VERBOSE);
  print $params->{'request'}->as_string() if ($params->{'request'} && $VERBOSE);
  print $params->{'response'}->as_string() if ($params->{'response'} && $VERBOSE);
  logEntry('message' => $message);
  logEntry('message' => $params->{'request'}->as_string()) if ($params->{'request'});
  logEntry('message' => $params->{'response'}->as_string()) if ($params->{'response'});
  exit 1;
}

sub logEntry {
  my $params = { @_ };

  my $message = $params->{'message'} || '';

  if ($message) {
    my $time = scalar localtime();
    print "[$time] $message\n" if ($VERBOSE);
    open LOG, ">>$logFile" or die "Couldn't open $logFile: $!";
    print LOG "[$time] $message\n";
    close LOG;
  }
}

sub checkMUAuthenticationToken {
  if ($token && time() < $tokenLastUpdate + (55 * 60)) {
    return 1;
  }

  getMUAuthenticationToken();
}

sub getMUAuthenticationToken {
  my $ua = LWP::UserAgent->new();

  my $request = HTTP::Request->new('POST', $wsServer . '/authentication/v1');
  my $data = {
      'username' => $config{'username'},
      'password' => $config{'password'},
      'type' => 'usernamePassword'
  };

  $request->content(to_json($data));
  $request->header('Content-Type', 'application/json');

  my $response = $ua->simple_request($request);

  # Do not log the request since it has username/password
  my $tokenInfo = extractResponseData('response' => $response);

  $token = $tokenInfo->{'token'};
  $tokenLastUpdate = time();

  print "Got new token ($token)\n" if ($VERBOSE);
}

