#!/usr/bin/perl

use strict;

use DBI;
use Data::Dumper;
use Getopt::Long;

my $count = 1000;

my $result = GetOptions('count' => \$count);

my $dbh = DBI->connect("DBI:Oracle:XE", "notsrv", "Hello123") || die DBI->errstr;

foreach my $table (qw(notsrv_email)) {
    $dbh->do(qq{
            delete from $table
        });
}

my %ratio = (
        '1' => .30,
        '2' => .25,
        '3' => .20,
        '4' => .15,
        '5' => .10,
    );


my %samplePoints;

foreach my $p (sort keys %ratio) {
    $samplePoints{$p} = ($ratio{$p} * $count) + ($p > 1 ? $samplePoints{$p - 1} : 0);
}

for (my $i = 0; $i < $count; $i++) {
  my $priority = 0;

  if ($i < $samplePoints{1}) { 
    $priority = 1; 
  } elsif ($i < $samplePoints{2}) { 
    $priority = 2;
  } elsif ($i < $samplePoints{3}) { 
    $priority = 3; 
  } elsif ($i < $samplePoints{4}) { 
    $priority = 4; 
  } else {
    $priority = 5;
  }

  my $s = {
      'status' => 'P',
      'priority' => $priority,
      'toAddr' => 'doej' . $i . '@example.com',
      'fromAddr' => 'info@example.com',
      'subject' => 'Important notice',
      'body' => 'Important stuff you need to do',
    };

    $dbh->do(q{
        insert into notsrv_email (id, status, priority, to_addr, from_addr, subject, body, scheduled_date, activity_date)
            values (notsrv_notification_id.nextval, ?, ?, ?, ?, ?, ?, sysdate, sysdate)
        }, undef, $s->{'status'}, $s->{'priority'}, $s->{'toAddr'},
            $s->{'fromAddr'}, $s->{'subject'}, $s->{'body'});
}


