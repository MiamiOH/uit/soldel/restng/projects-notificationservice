SET TERMOUT OFF
create tablespace notsrv_ts datafile 'notsrv_ts.dat' size 10M autoextend on;

create temporary tablespace notsrv_ts_temp tempfile 'notsrv_ts_temp.dat' size 5M autoextend on;

create user notsrv identified by Hello123
    default tablespace notsrv_ts
    temporary tablespace notsrv_ts_temp;

grant create session to notsrv;
grant create table to notsrv;
grant create sequence to notsrv;
grant create trigger to notsrv;
grant create view to notsrv;
grant create procedure to notsrv;
grant unlimited tablespace to notsrv;
