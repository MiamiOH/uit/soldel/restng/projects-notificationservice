declare

  v_authman_app_name varchar2(64) := null;
  v_authman_module_name varchar2(64) := null;
  v_authman_entity_name varchar2(64) := null;
  v_authman_grantkey varchar2(64) := null;

  v_application_id number;
  
begin

  v_authman_app_name := 'Notification Service';
  v_authman_module_name := 'Email';
  v_authman_entity_name := 'NOTESERV_WS_USER';
  v_authman_grantkey := 'create';
--
--   delete from a_authorization
--   where entity_id = (
--     select entity_id
--     from a_entity
--     where dn = v_authman_entity_name
--           and application_id = (
--       select application_id
--       from a_application
--       where name = v_authman_app_name
--     )
--   );
--
--   delete from a_entity
--   where application_id = (
--     select application_id
--     from a_application
--     where name = v_authman_app_name
--   );
--

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');
  -- Add a new grantkey.
  v_authman_grantkey := 'read';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  -- Add a new grantkey.
  v_authman_grantkey := 'update';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');
  -- Add a new grantkey.
  v_authman_grantkey := 'queue';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_module_name := 'SMS';

  v_authman_grantkey := 'read';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_grantkey := 'create';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');
  commit;

end;
/
