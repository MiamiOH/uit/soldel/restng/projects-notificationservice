ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD';
ALTER SESSION SET NLS_TIMESTAMP_FORMAT='YYYY-MM-DD HH24:MI:SS';

CREATE TABLE notsrv_sms
(
    id NUMBER NOT NULL,
    phone_number NVARCHAR2(25) NOT NULL,
    sender_id NUMBER NOT NULL,
    body NVARCHAR2(255) NOT NULL,
    priority NUMBER,
    error_message NVARCHAR2(1024),
    error_count NUMBER,
    end_date TIMESTAMP,
    scheduled_date TIMESTAMP,
    uuid NVARCHAR2(255) NOT NULL,
    status NVARCHAR2(10),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

ALTER TABLE notsrv_sms ADD CONSTRAINT notsrv_sms_pk PRIMARY KEY (id);

CREATE SEQUENCE notsrv_sms_SEQ
    INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE NOCACHE ORDER;

CREATE TABLE notsrv_sender
(
    id NUMBER NOT NULL,
    name NVARCHAR2(255),
    max_tries NUMBER DEFAULT 1,
    max_total_tries NUMBER DEFAULT 10,
    seconds_delay NUMBER DEFAULT 60,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT notsrv_sender_u_d94269 UNIQUE (name)
);

ALTER TABLE notsrv_sender ADD CONSTRAINT notsrv_sender_pk PRIMARY KEY (id);

CREATE SEQUENCE notsrv_sender_SEQ
    INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE NOCACHE ORDER;

CREATE TABLE notsrv_sendr_acct
(
    id NUMBER NOT NULL,
    ordinal NUMBER,
    from_number NVARCHAR2(25) NOT NULL,
    sender_id NUMBER NOT NULL,
    account_id NUMBER NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

ALTER TABLE notsrv_sendr_acct ADD CONSTRAINT notsrv_sendr_acct_pk PRIMARY KEY (id);

CREATE SEQUENCE notsrv_sendr_acct_SEQ
    INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE NOCACHE ORDER;

CREATE TABLE notsrv_account
(
    id NUMBER NOT NULL,
    name NVARCHAR2(255) NOT NULL,
    username NVARCHAR2(255),
    password NVARCHAR2(255),
    vendor_id NUMBER NOT NULL,
    miamiEmail NVARCHAR2(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT notsrv_account_u_d94269 UNIQUE (name)
);

ALTER TABLE notsrv_account ADD CONSTRAINT notsrv_account_pk PRIMARY KEY (id);

CREATE SEQUENCE notsrv_account_SEQ
    INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE NOCACHE ORDER;

CREATE TABLE notsrv_vendor
(
    id NUMBER NOT NULL,
    name NVARCHAR2(255) NOT NULL,
    className NVARCHAR2(255) NOT NULL,
    endPointLocation NVARCHAR2(255) NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT notsrv_vendor_u_d94269 UNIQUE (name)
);

ALTER TABLE notsrv_vendor ADD CONSTRAINT notsrv_vendor_pk PRIMARY KEY (id);

CREATE SEQUENCE notsrv_vendor_SEQ
    INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE NOCACHE ORDER;

ALTER TABLE notsrv_sms ADD CONSTRAINT notsrv_sms_fk_913652
    FOREIGN KEY (sender_id) REFERENCES notsrv_sender (id)
    ON DELETE CASCADE;

ALTER TABLE notsrv_sendr_acct ADD CONSTRAINT notsrv_sendr_acct_fk_913652
    FOREIGN KEY (sender_id) REFERENCES notsrv_sender (id)
    ON DELETE CASCADE;

ALTER TABLE notsrv_sendr_acct ADD CONSTRAINT notsrv_sendr_acct_fk_c8b2e8
    FOREIGN KEY (account_id) REFERENCES notsrv_account (id)
    ON DELETE CASCADE;

ALTER TABLE notsrv_account ADD CONSTRAINT notsrv_account_fk_49e83b
    FOREIGN KEY (vendor_id) REFERENCES notsrv_vendor (id)
    ON DELETE CASCADE;
