declare
  v_local_user_name varchar2(64) := null;
  v_local_user_pass varchar2(64) := null;

begin

  v_local_user_name := 'NOTESERV_WS_USER';
  v_local_user_pass := 'notifyme';

  insert into ws_authentication_local_users (username, algorithm, credential_hash, expiration_date, create_date, create_username)
    values (lower(v_local_user_name), 1, 
      lower(rawtohex(utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5(input_string=>v_local_user_pass)))),
      sysdate + 365, sysdate, 'tepeds');

end;
/