BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE email';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE notsrv_email';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE notsrv_email
   (
    id int,
    status varchar2(2),
    priority int,
    error_message varchar2(1024),
    error_count int,
    uuid varchar2(256),
    start_date date,
    end_date date,
    scheduled_date date,
    activity_date date,
    to_addr varchar2(256),
    from_addr varchar2(256),
    subject varchar2(256),
    body clob
   );

   COMMENT ON COLUMN notsrv_email.id IS 'Internal identification number of email object.';
   COMMENT ON COLUMN notsrv_email.status IS 'Notification status.';
   COMMENT ON COLUMN notsrv_email.priority IS 'Notification priority.';
   COMMENT ON COLUMN notsrv_email.error_message IS 'Error message associated with last status change.';
   COMMENT ON COLUMN notsrv_email.error_count IS 'Error counter ';
   COMMENT ON COLUMN notsrv_email.uuid IS 'User unique identifier (Miami uniqueid or other).';
   COMMENT ON COLUMN notsrv_email.start_date IS 'Date the notification record was added.';
   COMMENT ON COLUMN notsrv_email.end_date IS 'Date the notification record was set to a final status.';
   COMMENT ON COLUMN notsrv_email.scheduled_date IS 'Date the email is scheduled to be sent.';
   COMMENT ON COLUMN notsrv_email.activity_date IS 'Activity date of last action.';
   COMMENT ON COLUMN notsrv_email.to_addr IS 'To address of email notification.';
   COMMENT ON COLUMN notsrv_email.from_addr IS 'From address of email notification.';
   COMMENT ON COLUMN notsrv_email.subject IS 'Subject of email message.';
   COMMENT ON COLUMN notsrv_email.body IS 'Body of email message.';
   COMMENT ON TABLE notsrv_email IS 'Email notification values table';

  CREATE UNIQUE INDEX PK_notsrv_email ON notsrv_email (id) ;
  CREATE INDEX I_notsrv_status ON notsrv_email (status) ;
  CREATE INDEX I_notsrv_priority ON notsrv_email (priority) ;
  CREATE INDEX I_notsrv_uuid ON notsrv_email (uuid) ;
  CREATE INDEX I_notsrv_scheduled ON notsrv_email (scheduled_date) ;

  ALTER TABLE notsrv_email MODIFY (id NOT NULL ENABLE);
  ALTER TABLE notsrv_email MODIFY (to_addr NOT NULL ENABLE);
