<?php

namespace MiamiOH\NotificationService\Services\Model;

class EmailModel extends NotificationModel
{
    protected $possibleStatusCodes = array('P', 'S', 'E', 'F');
    protected $maxErrorCount = 50;
    protected $timeIncrement = 30;


    protected $keyMap = array(
        'id' => 'id',
        'status' => 'status',
        'priority' => 'priority',
        'error_message' => 'errorMessage',
        'error_count' => 'errorCount',
        'uuid' => 'uid',
        'start_date' => 'startDate',
        'end_date' => 'endDate',
        'scheduled_date' => 'scheduledDate',
        'to_addr' => 'toAddr',
        'from_addr' => 'fromAddr',
        'subject' => 'subject',
        'body' => 'body',
    );

    // @todo reconsider data design of notification & email table
    //   it may make more sense to just put the common elements
    //   in the type specific table
    public function __construct()
    {
        parent::__construct();

    }

    public function createSqlSelect($options)
    {
        // Default to all fields
        if (!isset($options['fields'])) {
            $options['fields'] = array_values($this->keyMap);
        }

        if (!is_array($options['fields'])) {
            throw new \Exception('Options for ' . __CLASS__ . '::createSqlSelect must contain a fields array');
        }

        $columnMap = $this->getKeyMapReversed();

        $selectColumns = array();
        foreach ($options['fields'] as $key) {
            if (!isset($columnMap[$key])) {
                throw new \Exception('Unknown attribute ' . __CLASS__ . '::' . $key);
            }

            if (strpos($columnMap[$key], '_date') !== false) {
                $selectColumns[] = 'to_char(' . $columnMap[$key] . ", 'YYYY-MM-DD HH24:MI:SS') as " . $columnMap[$key];
            } else {
                $selectColumns[] = $columnMap[$key];
            }
        }

        if (!$selectColumns) {
            throw new \Exception('No columns found to select');
        }

        $selectSql = 'select ' . implode(', ', $selectColumns) . "\n";
        $selectSql .= ' from notsrv_email' . "\n";
        $selectSql .= ' where 1 = 1';

        return $selectSql;
    }

    public function loadById($emailID, $options = array())
    {

        $loaded = false;
        $data = array();

        if ($this->exists($emailID)) {
            $this->dbh->setReturnLobs(true);

            $query = $this->createSqlSelect($options);
            $query .= ' and id = ?';
            $data = $this->dbh->queryfirstrow_assoc($query, $emailID);

            $loaded = $this->marshalFromDb($data);
        }

        return $loaded;
    }

    public function exists($emailID)
    {
        $exists = $this->dbh->queryfirstcolumn('
        select count(*)
          from notsrv_email
          where id = ?
      ', $emailID);

        return $exists ? true : false;
    }

    public function getModel()
    {
        // @todo add validation of model

        $model = array();

        foreach ($this->keyMap as $dbName => $modelName) {
            if (isset($this->attributes[$dbName])) {
                $model[$modelName] = $this->attributes[$dbName];
            }
        }

        return $model;
    }

    private function validateAttributes()
    {
        if (isset($this->attributes['status']) &&
            !in_array($this->attributes['status'], $this->possibleStatusCodes)
        ) {
            throw new \Exception('Invalid Status Code');
        }

        // Validate values
        if (isset($this->attributes['priority']) && (!is_numeric($this->attributes['priority']))) {
            throw new \Exception('Invalid Priority - value must be integer');
        }

        if (isset($this->attributes['to_addr']) && (!strpos($this->attributes['to_addr'], '@'))) {
            throw new \Exception('Invalid TO Email Address');
        }

        if (isset($this->attributes['from_addr']) && (!strpos($this->attributes['from_addr'], '@'))) {
            throw new \Exception('Invalid FROM Email Address');
        }
        if (!isset($this->attributes['to_addr'])) {
            throw new \Exception('Missing required "to_addr" value for email notification');
        }

    }


    public function save()
    {
        $this->validateAttributes();

        // @todo validate attributes before attempting to save
        if (!isset($this->attributes['id'])) {
            $this->id = $this->getNextId();
            $this->attributes['id'] = $this->id;

            // Supply defaults
            if (!isset($this->attributes['status'])) {
                $this->attributes['status'] = $this->defaultStatusCode;
            }

            if (!isset($this->attributes['priority'])) {
                $this->attributes['priority'] = $this->defaultPriority;
            }

            if (!isset($this->attributes['uuid'])) {
                $this->attributes['uuid'] = '';
            }

            if (!isset($this->attributes['from_addr'])) {
                $this->attributes['from_addr'] = '';
            }

            if (!isset($this->attributes['subject'])) {
                $this->attributes['subject'] = '';
            }

            if (!isset($this->attributes['body'])) {
                $this->attributes['body'] = '';
            }

            $sth = $this->dbh->prepare("
              insert into notsrv_email (id, status, priority, uuid, to_addr, from_addr, subject, body,
                  start_date, scheduled_date, activity_date)
                values (:ID, :STATUS, :PRIORITY, :UUID, :TOADDR, :FROMADDR, :SUBJECT, :BODY, 
                  to_date(:STARTDATE, 'YYYY-MM-DD HH24:MI:SS'), to_date(:SCHEDULEDDATE, 'YYYY-MM-DD HH24:MI:SS'),
                  sysdate)
            ");

            $sth->bind_by_name(':ID', $this->attributes['id']);
            $sth->bind_by_name(':STATUS', $this->attributes['status']);
            $sth->bind_by_name(':PRIORITY', $this->attributes['priority']);
            $sth->bind_by_name(':UUID', $this->attributes['uuid']);
            $sth->bind_by_name(':TOADDR', $this->attributes['to_addr']);
            $sth->bind_by_name(':FROMADDR', $this->attributes['from_addr']);
            $sth->bind_by_name(':SUBJECT', $this->attributes['subject']);
            $sth->bind_by_name(':BODY', $this->attributes['body']);
            $sth->bind_by_name(':STARTDATE', $this->attributes['start_date']);
            $sth->bind_by_name(':SCHEDULEDDATE', $this->attributes['scheduled_date']);

            $sth->execute();

        } elseif ($this->exists($this->attributes['id'])) {
            $updates = array();
            $values = array();

            foreach (array_keys($this->keyMap) as $key) {
                if (isset($this->attributes[$key])) {
                    if (strpos($key, '_date') !== false) {
                        $updates[] = $key . " = to_date( ?, 'YYYY-MM-DD HH24:MI:SS' ) ";
                    } else {
                        $updates[] = $key . ' = ?';
                    }
                    $values[] = $this->attributes[$key];
                }
            }

            if ($updates) {
                $updateString = implode(', ', $updates);
                $values[] = $this->attributes['id'];
                $this->dbh->perform("
            update notsrv_email set $updateString
              where id = ?
          ", $values);
            }
        } else {
            throw new \Exception('Cannot find id ' . $this->attributes['id'] . ' to update');
        }

        return true;
    }

    private function calculateScheduleDate($data)
    {
        if (isset($data['priority'])) {
            $calculatedIncrement = intval($data['priority']) * $this->timeIncrement;
        } elseif (isset($this->attributes['priority'])) {
            $calculatedIncrement = intval($this->attributes['priority']) * $this->timeIncrement;
        } else {
            $calculatedIncrement = intval($this->defaultPriority) * $this->timeIncrement;
        }
        if ((!isset($data['status'])) or ($data['status'] == 'P')) {
            $data['scheduledDate'] = date('Y-m-d H:i:s');
        }
        if ((isset($data['status'])) && ($data['status'] == 'E')) {
            $data['scheduledDate'] = date('Y-m-d H:i:s', strtotime('+' . $calculatedIncrement . ' minutes'));
        }
        return $data;
    }

    private function standardizeData($data)
    {
        // Status  being updated to Error (E) or Failed(F)
        if ((isset($data['status'])) && (in_array($data['status'], array('E', 'F')))) {
            //If Error startDate should not be updated & EndDate should not be set
            $data['endDate'] = '';
            unset($data['startDate']);

            if (!isset($this->attributes['error_count'])) {
                $data['errorCount'] = '1';
            } elseif ($this->maxErrorCount <= $this->attributes['error_count']) {
                $data['status'] = 'F';
                $data['errorCount'] = $this->maxErrorCount;
            } else {
                $data['errorCount'] = $this->attributes['error_count'] + 1;
            }
        } //Status Update - Mail Sent(S)
        elseif ((isset($data['status'])) && ($data['status'] == 'S')) {
            $data['endDate'] = date('Y-m-d H:i:s');
            // Start date should not be in the dataset
            unset($data['startDate']);
        } elseif ((!isset($data['status'])) or ($data['status'] == 'P')) {
            // StartDate should only be set when the entry is created
            $data['startDate'] = date('Y-m-d H:i:s');
        }
        $data = $this->calculateScheduleDate($data);

        return $data;
    }


    public function marshal($data, $source = 'model')
    {
        $data = $this->standardizeData($data);
        return parent::marshal($data, $source);
    }
}
