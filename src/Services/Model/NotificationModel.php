<?php

namespace MiamiOH\NotificationService\Services\Model;

use MiamiOH\RESTng\Connector\DatabaseFactory;

class NotificationModel
{

    protected $dbDataSourceName = 'NOTSRV_DB';
    protected $dbh;

    protected $keyMap = array();

    protected $id;

    protected $attributes = array();

    protected $defaultStatusCode = 'P';
    protected $defaultPriority = 5;

    public function __construct()
    {

    }

    public function setDatabase(DatabaseFactory $database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    protected function getKeyMapReversed()
    {
        return array_flip($this->keyMap);
    }

    // marshalFromDb assumes incoming db record
    public function marshalFromDb($data)
    {
        return $this->marshal($data, 'db');
    }

    // marshal is from incoming model
    public function marshal($data, $source = 'model')
    {
        if (!is_array($data)) {
            throw new \Exception('Argument to ' . __CLASS__ . '::marshal must be an array');
        }

        $this->attributes = array();
        $this->id = 0;

        $flipKeyMap = $this->getKeyMapReversed();

        foreach ($data as $key => $value) {
            $mapKey = $source === 'db' ? $key : $flipKeyMap[$key];
            $this->attributes[$mapKey] = $value;
        }

        if (isset($this->attributes['id'])) {
            $this->id = $this->attributes['id'];
        }

        return true;
    }

    protected function getNextId()
    {
        $id = $this->dbh->queryfirstcolumn('
        select notsrv_notification_id.nextval
          from dual
      ');

        return $id;
    }

}
