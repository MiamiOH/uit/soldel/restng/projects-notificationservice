<?php

namespace MiamiOH\NotificationService\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DataSource;
use MiamiOH\RESTng\Exception\DataSourceNotFound;

class WAS extends \MiamiOH\RESTng\Service
{
    /** @var  \MiamiOH\RESTng\Connector\ORMManager $ormManager */
    private $ormManager;

    /** @var  HttpClient $httpClient */
    private $httpClient;

    /** @var  \MiamiOH\RESTng\Connector\DataSourceFactory $dsFactory */
    private $dsFactory;

    public function setOrmManager($orm)
    {
        /** @var \MiamiOH\RESTng\Connector\ORMManager $orm */
        $this->ormManager = $orm;
        $this->connection = $this->ormManager->getWriteConnection('notification');
    }

    public function setHttpClient(HttpClient $client)
    {
        $this->httpClient = $client;
    }

    public function setDataSourceFactory($dsFactory)
    {
        /** @var  \MiamiOH\RESTng\Connector\DataSourceFactory $dsFactory */
        $this->dsFactory = $dsFactory;
    }

    public function getWAS()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $appKey = $request->getResourceParam('appKey');
        $uniqueId = $request->getResourceParam('uniqueId');

        $options = $request->getOptions();

        $payload = $this->getAppData($appKey, $uniqueId, $options);

        $response->setPayload($payload);
        $response->setStatus(App::API_OK);

        return $response;
    }

    public function createWAS()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $appKey = $request->getResourceParam('appKey');
        $uniqueId = $request->getResourceParam('uniqueId');

        $data = $request->getData();

        $dataSource = $this->getDataSource($appKey);

        $appFields = array_reduce(
            array_keys($data),
            function ($c, $name) use ($data) {
                $c[] = $name . '=' . $data[$name];
                return $c;
            },
            []
        );

        $url = $dataSource->getHost() . '/extapplog?' .
            'extapp=' . $dataSource->getUser() .
            '&ss=' . $dataSource->getPassword() .
            '&uid=' . $uniqueId .
            '&' . implode('&', $appFields);

        $createData = $this->parseXml($this->httpClient->get($url));

        if ($createData->{'status'} != 1) {
            $msg = 'Failed to create WAS message for ' . $uniqueId . ': ' . $createData->{'message'};
            $this->log->error($msg);
            throw new WasNotificationException($msg);
        }

        $options = [
            'daysPast' => 180,
            'attributes' => array_keys($data),
        ];

        $payload = $this->getAppData($appKey, $uniqueId, $options);

        $response->setPayload($payload);;
        $response->setStatus(App::API_CREATED);

        return $response;
    }

    private function getDataSource($appKey)
    {
        try {
            $dataSource = $this->dsFactory->getDataSource('notsrv_was_' . $appKey);
        } catch (DataSourceNotFound $e){
            $this->log->error($e->getMessage());
            throw new WasNotificationException($e->getMessage());
        }

        return $dataSource;
    }

    private function getAppData($appKey, $uniqueId, $options)
    {
        $dataSource = $this->getDataSource($appKey);

        $date = date('Ymd', strtotime('-' . $options['daysPast'] . ' days'));
        $url = $dataSource->getHost() . '/extappdata?' .
            'extapp=' . $dataSource->getUser() .
            '&ss=' . $dataSource->getPassword() .
            '&ids=' . $uniqueId .
            '&attributes=' . implode(',', $options['attributes']) .
            '&date=' . $date;

        $appData = $this->parseXml($this->httpClient->get($url));

        $userData = array_reduce(
            $options['attributes'],
            function ($c, $attr) use ($appData) {
                $c[$attr] = (string)$appData->{'record'}->{$attr};
                return $c;
            },
            []
        );

        return $userData;
    }

    private function parseXml($xml)
    {
        libxml_use_internal_errors(true);

        $data = simplexml_load_string($xml);

        if ($data === false) {
            $xmlErrors = array_reduce(
                libxml_get_errors(),
                function ($c, $error) {
                    $c[] = $error->message;
                    return $c;
                },
                []
            );

            libxml_clear_errors();

            $msg = 'Failed parsing XML response: ' . implode("\n", $xmlErrors);
            $this->log->error($msg);
            throw new WasNotificationException($msg);
        }

        return $data;
    }
}
