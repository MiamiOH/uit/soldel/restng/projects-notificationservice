<?php

namespace MiamiOH\NotificationService\Services\Collection;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Util\ModelFactory;

class NotificationCollection
{
    /**
     * @var string
     */
    protected $dbDataSourceName = 'NOTSRV_DB';
    /**
     * @var DBH
     */
    protected $dbh;

    /**
     * @var ModelFactory
     */
    protected $factory;

    public function __construct()
    {

    }

    public function setDatabase(DatabaseFactory $database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function setModelFactory(ModelFactory $factory)
    {
        $this->factory = $factory;
    }

    public function getModels($options = array())
    {
        return array();
    }

}
