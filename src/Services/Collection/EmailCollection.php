<?php

namespace MiamiOH\NotificationService\Services\Collection;

class EmailCollection extends NotificationCollection
{

    public function __construct()
    {
        parent::__construct();

    }

    public function getModels($options = array())
    {

        $model = $this->factory->newInstance('Email');

        $countQuery = '
        select count(*)
          from notsrv_email
          where 1 = 1
      ';

        $selectOptions = array();

        if (isset($options['partialRequestFields'])) {
            $selectOptions['fields'] = $options['partialRequestFields'];
        }

        $query = $model->createSqlSelect($selectOptions);

        $params = array();

        if (isset($options['status'])) {
            $statusCode = '';
            switch ($options['status']) {
                case 'pending':
                    $statusCode = 'P';
                    break;

                default:
                    throw new \Exception('Unknown notification status: ' . $options['status']);
            }

            $query .= '  and status = ?' . "\n";
            $countQuery .= '  and status = ?' . "\n";
            $params[] = $statusCode;

        }

        $totalRecords = $this->dbh->queryfirstcolumn($countQuery, $params);

        // @todo prepare query and hand off to parent method for paging
        $records = $this->dbh->queryall_array($query, $params);

        if ($options['isPaged']) {
            $minRowToFetch = $options['offset'];
            $maxRowToFetch = $minRowToFetch + $options['limit'] - 1;

            $query = "
          select *
            from ( select /*+ FIRST_ROWS(n) */
              a.*, ROWNUM rnum
                from ( $query ) a
                where ROWNUM <= ? )
          where rnum  >= ?";

            $sth = $this->dbh->prepare($query);

            $params[] = $maxRowToFetch;
            $params[] = $minRowToFetch;

        } else {
            $sth = $this->dbh->prepare($query);
        }

        $sth->setReturnLobs(true);
        $sth->execute($params);

        $models = array();
        while ($record = $sth->fetchrow_assoc()) {
            $model->marshalFromDb($record);
            $models[] = $model->getModel();
        }

        return array('total' => $totalRecords, 'models' => $models);
    }

    public function getPriorityPendingCounts()
    {
        $count = $this->dbh->queryall_list('
        select priority, count(*)
          from notsrv_email
          where status in(?, ?)
            and scheduled_date <= sysdate
          group by priority
          order by priority
      ', 'P', 'E');

        return $count;
    }

    public function getQueue($options = array())
    {
        if (!isset($options['count'])) {
            $options['count'] = 1000;
        }

        $model = $this->factory->newInstance('Email');

        // $priorityCounts will contain the actual count by priority
        $priorityCounts = $this->getPriorityPendingCounts();
        $totalPending = array_sum($priorityCounts);

        // The ratios are per priority of the total count
        $ratios = array(
            1 => .30,
            2 => .25,
            3 => .20,
            4 => .15,
            5 => .10,
        );

        // Use the ratios, requested count and real count to determine the
        // number of records to fetch for each priority. If there are fewer
        // records for a priority, roll the remainder to the next priority
        $limits = array();
        $pool = 0;
        do {
            foreach ($ratios as $priority => $ratio) {
                // The initial maximum to fetch for a given priority is the ratio of the total count
                $maxCount = $ratio * $options['count'];

                // Add our pool to the allowed maxCount. The pool is zero the first time
                // through, allowing potential selection from all priorities. Any spare
                // space is added to the pool for subsequent use.
                $maxCount += $pool;

                // The limit is the lesser of the calculated ratio or actual count
                $limit = min($priorityCounts[$priority], $maxCount);

                $limits[$priority] = $limit;

                // Find the variance
                //   negative = under resourced, notifications for this priority remain
                //   zero     = exactly matched
                //   positive = over resourced, can select more notifications
                $variance = $maxCount - $priorityCounts[$priority];

                // Assign any over resourced amount to our pool
                if ($variance > 0) {
                    $pool += $variance;
                }

            }

            // Loop while there is a pool of open limits and the aggregate limits
            // do not exceed the requested count.
        } while ($pool > 0 && array_sum($limits) < $totalPending);

        $selectOptions = array();

        if (isset($options['partialRequestFields'])) {
            $selectOptions['fields'] = $options['partialRequestFields'];
        }

        $selectQuery = $model->createSqlSelect($selectOptions);

        // Using placeholders here will cause problems with the accumulation
        // of param values for the query.
        $selectQuery .= "\n and status in ('P', 'E')\n";

        $priorityQueries = array();
        $queryParams = array();
        foreach (array_keys($ratios) as $priority) {
            $priorityQueries[$priority] = "
          select /*+ FIRST_ROWS(n) */
                        a.*, ROWNUM rnum
                          from ( 
                              $selectQuery
                                and priority = ?
                                and scheduled_date <= sysdate  
                              order by scheduled_date           
                          ) a
                          where ROWNUM <= ? 
      ";

            $queryParams[] = $priority;
            $queryParams[] = $limits[$priority];

        }

        // We must use UNION ALL to avoid issues with CLOBs
        $unionedQuery = implode("\nunion all\n", $priorityQueries);

        $sth = $this->dbh->prepare($unionedQuery);

        $sth->setReturnLobs(true);
        $sth->execute($queryParams);

        $models = array();
        while ($record = $sth->fetchrow_assoc()) {
            $model->marshalFromDb($record);
            $models[] = $model->getModel();
        }

        return $models;
    }
}
