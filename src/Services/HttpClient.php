<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 1/2/18
 * Time: 9:58 PM
 */

namespace MiamiOH\NotificationService\Services;


class HttpClient
{
    public function get($url)
    {
        return file_get_contents($url);
    }
}
