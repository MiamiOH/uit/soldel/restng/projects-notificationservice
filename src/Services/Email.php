<?php

namespace MiamiOH\NotificationService\Services;

use MiamiOH\RESTng\App;

class Email extends \MiamiOH\RESTng\Service
{

    /** @var  \MiamiOH\RESTng\Util\ModelFactory $factory */
    private $factory;

    /** @var  \MiamiOH\NotificationService\Services\Collection\EmailCollection $collection */
    private $collection;

    public function setModelFactory($factory)
    {
        $this->factory = $factory;
    }

    public function setEmailCollection($collection)
    {
        $this->collection = $collection;
    }

    public function getEmailById()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'read');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $emailId = $request->getResourceParam('id');

        $loadOptions['isPartial'] = $request->isPartial();
        if ($loadOptions['isPartial']) {
            $loadOptions['fields'] = $request->getPartialRequestFields();
        }

        $notification = $this->factory->newInstance('Email');
        $found = $notification->loadById($emailId, $loadOptions);

        if ($found) {
            $message = $notification->getModel();
            $response->setPayload($message);
        } else {
            $response->setStatus(App::API_NOTFOUND);
        }

        return $response;
    }

    public function createEmail()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'create');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $data = $request->getData();

        $notification = $this->factory->newInstance('Email');

        if (!(isset($data['dataType']) && in_array($data['dataType'], array('model', 'collection')))) {
            throw new \Exception('createEmail payload must declare a correct dataType ("model" or "collection")');
        }

        if (!isset($data['data'])) {
            throw new \Exception('createEmail payload must contain a data element');
        }

        if ($data['dataType'] === 'model') {
            $notification->marshal($data['data']);

            // @todo wrap save in try/catch and handle exceptions
            $notification->save();

            $message = $notification->getModel();

            $response->setStatus(App::API_CREATED);
            $response->setPayload($message);
        } elseif ($data['dataType'] === 'collection') {
            $createResults = array();
            for ($i = 0; $i < count($data['data']); $i++) {
                try {
                    $notification->marshal($data['data'][$i]);
                    $notification->save();
                    $createResults[] = array('code' => App::API_CREATED, 'message' => '',
                        'model' => $notification->getModel());
                } catch (\Exception $e) {
                    $createResults[] = array('code' => App::API_FAILED, 'message' => $e->getMessage(),
                        'model' => array());
                }
            }

            $response->setStatus(App::API_CREATED);
            $response->setPayload($createResults);
        } else {
            // Really should never get here, but don't want to risk returning an OK
            $response->setStatus(App::API_FAILED);
        }

        return $response;
    }

    public function updateEmailModel()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'update');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $data = $request->getData();
        $notification = $this->factory->newInstance('Email');
        $notification->loadById($data['id']);
        // var_dump($notification);
        $notification->marshal($data);

        // @todo wrap save in try/catch and handle exceptions
        $notification->save();
        // var_dump($notification);

        $response->setStatus(App::API_OK);

        return $response;
    }

    public function updateEmailCollection()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'update');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $data = $request->getData();
        $notification = $this->factory->newInstance('Email');

        $updateResults = array();
        for ($i = 0; $i < count($data); $i++) {
            try {
                $notification->marshal($data[$i]);
                $notification->save();
                $updateResults[] = array('code' => App::API_OK, 'message' => '');
            } catch (\Exception $e) {
                $updateResults[] = array('code' => App::API_FAILED, 'message' => $e->getMessage());
            }
        }

        $response->setStatus(App::API_OK);
        $response->setPayload($updateResults);

        return $response;
    }

    public function getCollection()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'read');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $options = $request->getOptions();

        $collectionOptions = array();

        if (!isset($options['status'])) {
            $collectionOptions['status'] = 'pending';
        }

        $collectionOptions['isPaged'] = $request->isPaged();

        if ($collectionOptions['isPaged']) {
            $collectionOptions['offset'] = $request->getOffset();
            $collectionOptions['limit'] = $request->getLimit();
        }

        $collectionOptions['isPartial'] = $request->isPartial();

        if ($collectionOptions['isPartial']) {
            $collectionOptions['partialRequestFields'] = $request->getPartialRequestFields();
            $collectionOptions['subObjects'] = $request->getSubObjects();
        }

        $modelCollection = $this->collection->getModels($collectionOptions);

        if ($collectionOptions['isPaged']) {
            $response->setTotalObjects($modelCollection['total']);
        }

        $response->setPayload($modelCollection['models']);

        return $response;
    }

    public function getQueue()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'queue');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $options = $request->getOptions();

        $queueOptions = array();

        if (isset($options['max'])) {
            $queueOptions['count'] = $options['max'];
        }

        $queueCollection = $this->collection->getQueue($queueOptions);

        $response->setPayload($queueCollection);

        return $response;
    }

}
