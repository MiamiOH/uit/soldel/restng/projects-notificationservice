<?php


namespace MiamiOH\NotificationService\Util;


use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;

class IdProvider
{
    /** @var DBH */
    private $dbh;

    public function setDatabase(DatabaseFactory $database)
    {
        $this->dbh = $database->getHandle('NOTSRV_DB');
    }

    public function nextEmailId(): int
    {
        return $this->dbh->queryfirstcolumn('select notsrv_notification_id.nextval from dual');
    }
}
