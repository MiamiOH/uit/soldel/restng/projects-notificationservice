<?php


namespace MiamiOH\NotificationService\Util;


class HttpClient
{
    public function get($url)
    {
        return file_get_contents($url);
    }
}