<?php


namespace MiamiOH\NotificationService\Was;


use MiamiOH\NotificationService\Exception\NotFoundException;
use MiamiOH\NotificationService\Exception\NotificationException;
use MiamiOH\NotificationService\Exception\WasNotificationException;
use MiamiOH\NotificationService\Util\HttpClient;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use MiamiOH\RESTng\Exception\DataSourceNotFound;

class WasAgent
{
    /** @var HttpClient */
    private $httpClient;

    /** @var DataSourceFactory */
    private $dataSourceFactory;

    public function setHttpClient(HttpClient $client): void
    {
        $this->httpClient = $client;
    }

    public function setDataSourceFactory(DataSourceFactory $factory): void
    {
        $this->dataSourceFactory = $factory;
    }

    /**
     * @param string $appKey
     * @param string $uid
     * @param array $options
     * @return array
     * @throws NotFoundException
     * @throws NotificationException
     */
    public function getAppMessageForUser(string $appKey, string $uid, array $options = []): array
    {
        $dataSource = $this->getDataSource($appKey);

        $date = date('Ymd', strtotime('-' . $options['daysPast'] . ' days'));

        $url = $dataSource->getHost() . '/extappdata?' .
            'extapp=' . $dataSource->getUser() .
            '&ss=' . $dataSource->getPassword() .
            '&ids=' . $uid .
            '&attributes=' . implode(',', $options['attributes']) .
            '&date=' . $date;

        $xml = $this->httpClient->get($url);

        $data = new \SimpleXMLElement($xml);

        $values = [];
        foreach ($options['attributes'] as $attribute) {
            $values[$attribute] = (string) $data->record->{$attribute}[0];
        }

        return $values;
    }

    public function createAppMessageForUser(string $appKey, string $uid, array $attributes): void
    {
        $dataSource = $this->getDataSource($appKey);

        $appFields = implode('&', array_reduce(
            array_keys($attributes),
            static function ($c, $name) use ($attributes) {
                $c[] = $name . '=' . $attributes[$name];
                return $c;
            },
            []
        ));

        $url = $dataSource->getHost() . '/extapplog?' .
            'extapp=' . $dataSource->getUser() .
            '&ss=' . $dataSource->getPassword() .
            '&uid=' . $uid .
            '&' . $appFields;

        $xml = $this->httpClient->get($url);

        $data = new \SimpleXMLElement($xml);

        if (empty($data->{'status'})) {
            throw new WasNotificationException(sprintf('Message for %s could not be created in %s', $uid, $appKey));
        }
    }

    /**
     * @param $appKey
     * @return \MiamiOH\RESTng\Connector\DataSource
     * @throws NotificationException
     */
    private function getDataSource($appKey)
    {
        try {
            $dataSource = $this->dataSourceFactory->getDataSource('notsrv_was_' . $appKey);
        } catch (DataSourceNotFound $e){
            throw new NotificationException($e->getMessage());
        }

        return $dataSource;
    }
}