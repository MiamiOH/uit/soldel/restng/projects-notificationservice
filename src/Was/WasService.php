<?php

namespace MiamiOH\NotificationService\Was;

use MiamiOH\NotificationService\Exception\NotFoundException;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;

class WasService extends \MiamiOH\RESTng\Service
{
    /** @var WasAgent */
    private $agent;

    public function setWasAgent(WasAgent $agent): void
    {
        $this->agent = $agent;
    }

    public function getWasMessage(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $appKey = $request->getResourceParam('appKey');
        $uniqueId = $request->getResourceParam('uniqueId');

        $options = $request->getOptions();

        try {
            $payload = $this->agent->getAppMessageForUser($appKey, $uniqueId, $options);
        } catch (NotFoundException $e) {
            $response->setStatus(App::API_NOTFOUND);
            return $response;
        }

        $response->setPayload($payload);
        $response->setStatus(App::API_OK);

        return $response;
    }

    public function createWasMessage(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $appKey = $request->getResourceParam('appKey');
        $uniqueId = $request->getResourceParam('uniqueId');

        $data = $request->getData();

        $this->agent->createAppMessageForUser($appKey, $uniqueId, $data);

        try {
            $options = [
                'daysPast' => 30,
                'attributes' => array_keys($data)
            ];
            $payload = $this->agent->getAppMessageForUser($appKey, $uniqueId, $options);
        } catch (NotFoundException $e) {
            $response->setStatus(App::API_NOTFOUND);
            return $response;
        }

        $response->setPayload($payload);
        $response->setStatus(App::API_CREATED);

        return $response;
    }
}
