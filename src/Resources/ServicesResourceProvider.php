<?php

namespace MiamiOH\NotificationService\Resources;

use MiamiOH\NotificationService\Email\EmailRepository;
use MiamiOH\NotificationService\Email\EmailService;
use MiamiOH\NotificationService\Util\HttpClient;
use MiamiOH\NotificationService\Util\IdProvider;
use MiamiOH\NotificationService\Was\WasAgent;
use MiamiOH\NotificationService\Was\WasService;
use MiamiOH\RESTng\Util\ResourceProvider;

class ServicesResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'Notification',
            'description' => 'Resources for Notification Messages'
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'NotificationIdProvider',
            'class' => IdProvider::class,
            'description' => 'Provide means to get next available object ID',
            'set' => [
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ]
        ));

        $this->addService(array(
            'name' => 'NotificationEmailRepository',
            'class' => EmailRepository::class,
            'description' => 'Provide Email data storage services.',
            'set' => array(
                'idProvider' => array('type' => 'service', 'name' => 'NotificationIdProvider'),
            ),
        ));

        $this->addService(array(
            'name' => 'NotificationEmailService',
            'class' => EmailService::class,
            'description' => 'Provide Email services.',
            'set' => array(
                'emailRepository' => array('type' => 'service', 'name' => 'NotificationEmailRepository'),
                'maxMessageBodySizeBytes' => [
                    'type' => 'scalar',
                    'value' => env('NOTIFICATION_SERVICE_EMAIL_MAX_BODY_SIZE_BYTES', 1024 * 1024)
                ],
            ),
        ));

        $this->addService(array(
            'name' => 'NotificationHTTPClient',
            'class' => HttpClient::class,
            'description' => 'Provide basic http functionality',
        ));

        $this->addService(array(
            'name' => 'NotificationWasAgent',
            'class' => WasAgent::class,
            'description' => 'Handle WAS interactions.',
            'set' => array(
                'httpClient' => array('type' => 'service', 'name' => 'NotificationHTTPClient'),
                'dataSourceFactory' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),
            ),
        ));

        $this->addService(array(
            'name' => 'NotificationWasService',
            'class' => WasService::class,
            'description' => 'Provide WAS services.',
            'set' => array(
                'wasAgent' => array('type' => 'service', 'name' => 'NotificationWasAgent'),
            ),
        ));
    }

    public function registerResources(): void
    {

    }
}
