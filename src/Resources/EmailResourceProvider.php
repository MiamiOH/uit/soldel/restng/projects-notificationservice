<?php

namespace MiamiOH\NotificationService\Resources;

use MiamiOH\RESTng\Util\ResourceProvider;

class EmailResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {


    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'notification.v1.email.message.id',
            'description' => 'Get an email notification by id.',
            'tags' => array('Notification'),
            'pattern' => '/notification/v1/email/message/:id',
            'service' => 'NotificationEmailService',
            'method' => 'getEmailMessage',
            'returnType' => 'model',
            'isPartialable' => true,
            'params' => array(
                'id' => array('description' => 'A notification ID'),
            ),
            'middleware' => array(
                'authenticate' => array(),
            ),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'notification.v1.email.message.create',
            'description' => 'Create a new notification email.',
            'tags' => array('Notification'),
            'pattern' => '/notification/v1/email/message',
            'service' => 'NotificationEmailService',
            'method' => 'createEmailMessage',
            'returnType' => 'model',
            'middleware' => array(
                'authenticate' => array(),
            ),
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'notification.v1.email.message.update',
            'description' => 'Update an existing notification email.',
            'tags' => array('Notification'),
            'pattern' => '/notification/v1/email/message',
            'service' => 'NotificationEmailService',
            'method' => 'updateEmailMessages',
            'returnType' => 'none',
            'middleware' => array(
                'authenticate' => array(),
            ),
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'notification.v1.email.message.update.id',
            'description' => 'Update an existing notification email.',
            'tags' => array('Notification'),
            'pattern' => '/notification/v1/email/message/:id',
            'service' => 'NotificationEmailService',
            'method' => 'updateEmailMessage',
            'returnType' => 'none',
            'params' => array(
                'id' => array('description' => 'A notification ID'),
            ),
            'middleware' => array(
                'authenticate' => array(),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'notification.v1.email.collection',
            'description' => 'Get a collection of email notifications.',
            'tags' => array('Notification'),
            'pattern' => '/notification/v1/email/collection',
            'service' => 'NotificationEmailService',
            'method' => 'getEmailMessageCollection',
            'returnType' => 'collection',
            'isPageable' => true,
            'isPartialable' => true,
            'middleware' => array(
                'authenticate' => array(),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'notification.v1.email.queue',
            'description' => 'Get a collection of email notifications from the queue.',
            'tags' => array('Notification'),
            'pattern' => '/notification/v1/email/queue',
            'service' => 'NotificationEmailService',
            'method' => 'getPendingEmailMessages',
            'returnType' => 'collection',
            'options' => array(
                'max' => array('description' => 'The maximum number of entries to return')
            ),
            'middleware' => array(
                'authenticate' => array(),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'notification.v1.email.queue.status',
            'description' => 'Get the status summary for the email queue.',
            'tags' => array('Notification'),
            'pattern' => '/notification/v1/email/queue/status',
            'service' => 'NotificationEmailService',
            'method' => 'getEmailQueueStatus',
            'returnType' => 'model',
            'options' => array(
                'span_minutes' => array('description' => 'The number of minutes in the summary span'),
                'oldest_message_minutes' => array('description' => 'The oldest message threshold in minutes'),
            ),
        ));
    }
}
