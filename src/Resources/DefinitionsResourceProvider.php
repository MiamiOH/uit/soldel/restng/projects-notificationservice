<?php

namespace MiamiOH\NotificationService\Resources;

use MiamiOH\RESTng\Util\ResourceProvider;

class DefinitionsResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'WASMessage.Record.Response',
            'type' => 'object',
            'properties' => array(
                'app_data_1' => array(
                    'type' => 'string',
                ),
                'app_data_2' => array(
                    'type' => 'string',
                ),
                'app_data_3' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'WASMessage.Record.Request',
            'type' => 'object',
            'required' => ['AppName', 'UniqueId', 'Data'],
            'properties' => array(
                'app_data_1' => array(
                    'type' => 'string',
                ),
                'app_data_2' => array(
                    'type' => 'string',
                ),
                'app_data_3' => array(
                    'type' => 'string',
                )
            )
        ));
    }

    public function registerServices(): void
    {


    }

    public function registerResources(): void
    {

    }
}
