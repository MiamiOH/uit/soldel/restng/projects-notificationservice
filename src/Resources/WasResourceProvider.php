<?php

namespace MiamiOH\NotificationService\Resources;

use MiamiOH\RESTng\Util\ResourceProvider;

class WasResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {


    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'notification.v1.was.message.id',
            'description' => 'Get a WAS notification by id.',
            'tags' => array('Notification'),
            'pattern' => '/notification/v1/was/message/:appKey/:uniqueId',
            'service' => 'NotificationWasService',
            'method' => 'getWasMessage',
            'returnType' => 'model',
            'params' => array(
                'appKey' => array('description' => 'A key identifying the app to target'),
                'uniqueId' => array('description' => 'A user uniqueId'),
            ),
            'options' => array(
                'daysPast' => array(
                    'required' => true,
                    'description' => 'number of days into the past to fetch data'
                ),
                'attributes' => array(
                    'required' => true,
                    'type' => 'list',
                    'description' => 'list of attributes to return'
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'Notification Service',
                        'module' => 'WAS',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'notification.v1.was.message.create',
            'description' => 'Create a new WAS notification.',
            'tags' => array('Notification'),
            'pattern' => '/notification/v1/was/message/:appKey/:uniqueId',
            'service' => 'NotificationWasService',
            'method' => 'createWasMessage',
            'returnType' => 'model',
            'params' => array(
                'appKey' => array('description' => 'A key identifying the app to target'),
                'uniqueId' => array('description' => 'A user uniqueId'),
            ),
            'body' => array(
                'description' => 'Hash of WAS application specific attributes',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/WASMessage.Record.Request',
                )
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'Notification Service',
                        'module' => 'WAS',
                        'key' => 'create'
                    ),
                ),
            ),
        ));
    }
}