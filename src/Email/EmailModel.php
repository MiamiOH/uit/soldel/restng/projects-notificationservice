<?php


namespace MiamiOH\NotificationService\Email;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EmailModel
 * @package MiamiOH\NotificationService\Email
 *
 * @property int $id
 * @property string $status
 * @property int $priority
 * @property string $error_message
 * @property int $error_count
 * @property string $uuid
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property Carbon $scheduled_date
 * @property string $to_addr
 * @property string $from_addr
 * @property string $subject
 * @property string $body
 * @property Carbon $activity_date
 */
class EmailModel extends Model implements Email
{
    protected $connection = 'NOTSRV_DB';

    protected $table = 'notsrv_email';

    public $incrementing = false;

    public $timestamps = false;

    protected $dateFormat = 'Y-m-d H:i:s';

    protected $dates = ['start_date', 'end_date', 'scheduled_date'];

    protected $fillable = [
        'id',
        'status',
        'priority',
        'error_message',
        'error_count',
        'uuid',
        'start_date',
        'end_date',
        'scheduled_date',
        'to_addr',
        'from_addr',
        'subject',
        'body',
        'activity_date',
    ];

    public function save(array $options = [])
    {
        $this->activity_date = Carbon::now();

        return parent::save($options);
    }


    public function toData(): array
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'priority' => $this->priority,
            'startDate' => $this->start_date->toIso8601ZuluString(),
            'endDate' => empty($this->end_date) ? null : $this->end_date->toIso8601ZuluString(),
            'scheduledDate' => $this->scheduled_date->toIso8601ZuluString(),
            'toAddr' => $this->to_addr,
            'fromAddr' => $this->from_addr,
            'subject' => $this->subject,
            'body' => $this->body,
        ];
    }

    public function fromData(array $data): void
    {
        $this->id = $data['id'];
        $this->status = $data['status'];
        $this->priority = (int) $data['priority'];
        $this->start_date = $data['startDate'];
        $this->end_date = $data['endDate'] ?? null;
        $this->scheduled_date = $data['scheduledDate'];
        $this->to_addr = $data['toAddr'];
        $this->from_addr = $data['fromAddr'];
        $this->subject = $data['subject'];
        $this->body = $data['body'];
    }
}