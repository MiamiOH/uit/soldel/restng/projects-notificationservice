<?php


namespace MiamiOH\NotificationService\Email;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use MiamiOH\NotificationService\Exception\NotFoundException;
use MiamiOH\NotificationService\Util\IdProvider;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;

class EmailRepository
{
    public const DEFAULT_STATUS = 'P';
    public const DEFAULT_PRIORITY = 5;
    public const DELAY_MINUTES_FACTOR = 5;
    public const MAX_FAILURES = 5;
    public const QUEUE_STATUS_OK = 'ok';
    public const QUEUE_STATUS_WARNING = 'warning';
    public const QUEUE_STATUS_CRITICAL = 'critical';
    public const QUEUE_STATUS_DEFAULT_SPAN = 60;
    public const QUEUE_STATUS_OLDEST_MESSAGE = 60;

    /** @var IdProvider */
    private $idProvider;

    public function setIdProvider(IdProvider $provider): void
    {
        $this->idProvider = $provider;
    }

    /**
     * @param int $id
     * @return Email
     * @throws NotFoundException
     */
    public function getEmailMessage(int $id): Email
    {
        try {
            return EmailModel::where('id', '=', $id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new NotFoundException(sprintf('Email message with id %s not found', $id));
        }
    }

    /**
     * @param array $data
     * @return Email
     * @throws BadRequest
     */
    public function createEmailMessage(array $data): Email
    {
        $data['id'] = $this->idProvider->nextEmailId();

        if (empty($data['status'])) {
            $data['status'] = self::DEFAULT_STATUS;
        }

        if (empty($data['priority'])) {
            $data['priority'] = self::DEFAULT_PRIORITY;
        }

        // Start date cannot be provided when creating a message
        $data['startDate'] = Carbon::now();

        // Scheduled date is now unless provided
        if (empty($data['scheduledDate'])) {
            $data['scheduledDate'] = Carbon::now();
        } else {
            $data['scheduledDate'] = Carbon::parse($data['scheduledDate']);
        }

        $this->ensureData($data);

        $model = new EmailModel();
        $model->fromData($data);
        $model->save();

        return $model;
    }

    /**
     * @param array $data
     * @throws BadRequest
     */
    private function ensureData(array $data): void
    {
        if (!in_array($data['status'], ['P', 'S', 'E', 'F'])) {
            throw new BadRequest(sprintf('Invalid status %s', $data['status']));
        }

        if (!is_numeric($data['priority'])) {
            throw new BadRequest(sprintf('Invalid priority %s', $data['priority']));
        }

        if (empty($data['toAddr'])) {
            throw new BadRequest('Missing required to address');
        }

        if (!filter_var($data['toAddr'], FILTER_VALIDATE_EMAIL)) {
            throw new BadRequest(sprintf('Invalid to address %s', $data['toAddr']));
        }

        if (!empty($data['fromAddr']) && !filter_var($data['fromAddr'], FILTER_VALIDATE_EMAIL)) {
            throw new BadRequest(sprintf('Invalid from address %s', $data['fromAddr']));
        }
    }

    /**
     * @param int $id
     * @param array $data
     * @throws NotFoundException
     * @throws BadRequest
     */
    public function updateEmailMessage(int $id, array $data): void
    {
        try {
            /** @var EmailModel $model */
            $model = EmailModel::where('id', '=', $id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new NotFoundException(sprintf('Email message with id %s not found', $id));
        }

        if (in_array($model->status, ['S', 'F'])) {
            throw new BadRequest(sprintf('Cannot update message in status %s', $model->status));
        }

        if ($data['status'] === 'E') {
            ++$model->error_count;
            $model->error_message = $data['errorMessage'] ?? 'Unknown error';
            if ($model->error_count >= self::MAX_FAILURES) {
                $data['status'] = 'F';
            } else {
                $model->scheduled_date = Carbon::now()->addMinutes($model->priority * self::DELAY_MINUTES_FACTOR);
            }
        } elseif ($data['status'] === 'S') {
            $model->end_date = Carbon::now();
            $model->error_message = null;
        } else {
            throw new BadRequest(sprintf('Invalid status %s', $data['status']));
        }

        $model->status = $data['status'];

        $model->save();
    }

    /**
     * @param array $messages
     * @return Collection
     */
    public function updateEmailMessages(array $messages): Collection
    {
        $updates = new Collection($messages);

        return $updates->map(function (array $messageData) {
            try {
                $this->updateEmailMessage($messageData['id'], $messageData);
            } catch (NotFoundException $e) {
                return ['status' => App::API_NOTFOUND];
            } catch (BadRequest $e) {
                return ['status' => App::API_BADREQUEST];
            }
            return ['status' => App::API_OK];
        });
    }

    /**
     * @param array $options
     * @return Collection
     * @throws BadRequest
     */
    public function getPendingEmailMessages(array $options = []): Collection
    {
        $limit = $options['max'] ?? 1000;

        if ($limit > 1000) {
            throw new BadRequest(sprintf('Limit %s exceeds allowed limit of 1000', $limit));
        }

        return EmailModel::whereIn('status', ['P', 'E'])
            ->where('scheduled_date', '<=', Carbon::now()->format('Y-m-d H:i:s'))
            ->orderBy('priority')
            ->orderBy('scheduled_date')
            ->limit($limit)
            ->get();
    }

    public function getEmailQueueStatus(array $options = []): array
    {
        $spanMinutes = $options['span_minutes'] ?? self::QUEUE_STATUS_DEFAULT_SPAN;
        $oldestMessageMinutes = $options['oldest_message_minutes'] ?? self::QUEUE_STATUS_OLDEST_MESSAGE;

        $now = Carbon::now()->toImmutable();
        $spanStartTime = $now->subMinutes($spanMinutes);

        // Total pending messages
        $totalPending = EmailModel::where('status', '=', 'P')->count();

        // Total in span
        $spanMessageCount = EmailModel::whereBetween('scheduled_date', [$spanStartTime, $now])->count();

        // Total sent in span
        $spanSentCount = EmailModel::where('status', '=', 'S')
            ->whereBetween('scheduled_date', [$spanStartTime, $now])
            ->count();

        // Error count in last N minutes (option)
        $spanErrorCount = EmailModel::where('status', '=', 'E')
            ->whereBetween('scheduled_date', [$spanStartTime, $now])
            ->count();

        // Oldest, highest priority message
        try {
            $oldestPending = EmailModel::where('status', '=', 'P')
                ->orderBy('priority')
                ->orderBy('scheduled_date')
                ->take(1)
                ->firstOrFail();

            $oldestPendingMinutes = $oldestPending->scheduled_date->diffInMinutes(Carbon::now());
        } catch (ModelNotFoundException $e) {
            $oldestPendingMinutes = 0;
        }

        // determine overall status
        $status = self::QUEUE_STATUS_OK;
        $message = '';

        // if error count != 0, warning
        if ($spanMessageCount && $spanErrorCount / $spanMessageCount === 1) {
            $status = self::QUEUE_STATUS_CRITICAL;
            $message = sprintf('All %s messages have failed since %s', $spanErrorCount, $spanStartTime->toDateTimeString());
        }
        // if oldest pending > 60 minutes, critical
        elseif ($oldestPendingMinutes >= $oldestMessageMinutes) {
            $status = self::QUEUE_STATUS_CRITICAL;
            $message = sprintf('Oldest pending messages exceeds critical threshold (%s minutes): %s minutes',$oldestMessageMinutes,  $oldestPendingMinutes);
        }
        // if oldest pending > span minutes, warning
        elseif ($oldestPendingMinutes >= $spanMinutes) {
            $status = self::QUEUE_STATUS_WARNING;
            $message = sprintf('Oldest pending message exceeds span (%s minutes): %s', $spanMinutes, $oldestPendingMinutes);
        }
        // if error rate = 1, critical
        elseif ($spanErrorCount !== 0) {
            $status = self::QUEUE_STATUS_WARNING;
            $message = sprintf('%s messages out of %s total with errors in span (%s minutes)', $spanErrorCount, $spanMessageCount, $spanMinutes);
        }

        return [
            'total_pending' => $totalPending,
            'span_start' => $spanStartTime->toDateTimeString(),
            'span_message_count' => $spanMessageCount,
            'span_sent_count' => $spanSentCount,
            'span_error_count' => $spanErrorCount,
            'oldest_pending_minutes' => $oldestPendingMinutes,
            'status' => $status,
            'message' => $message,
        ];
    }
}
