<?php

namespace MiamiOH\NotificationService\Email;

use MiamiOH\NotificationService\Exception\NotFoundException;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Exception\InvalidArgumentException;
use MiamiOH\RESTng\Util\Response;

class EmailService extends \MiamiOH\RESTng\Service
{
    /** @var EmailRepository */
    private $emailRepository;

    private $maxBodySizeBytes = 1024 * 1024; // 1 MB

    public function setEmailRepository(EmailRepository $repository): void
    {
        $this->emailRepository = $repository;
    }

    public function setMaxMessageBodySizeBytes(int $bytes): void
    {
        $this->maxBodySizeBytes = $bytes;
    }

    public function getEmailMessage(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'read');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $id = $request->getResourceParam('id');

        try {
            $message = $this->emailRepository->getEmailMessage($id);
        } catch (NotFoundException $e) {
            $response->setStatus(App::API_NOTFOUND);
            return $response;
        }

        $response->setPayload($message->toData());
        $response->setStatus(App::API_OK);

        return $response;
    }

    public function createEmailMessage()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'create');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $data = $request->getData();

        if ($data['dataType'] === 'model') {
            if (strlen($data['data']['body']) > $this->maxBodySizeBytes) {
                $this->serviceLog->warn(sprintf(
                    'Message body exceeds max allowed bytes, %s: from: "%s", to: "%s", subject: "%s"',
                    $this->maxBodySizeBytes,
                    $data['data']['fromAddr'],
                    $data['data']['toAddr'],
                    $data['data']['subject']
                ));
                throw new BadRequest(sprintf('Message body exceeds max allowed bytes, %s', $this->maxBodySizeBytes));
            }

            $message = $this->emailRepository->createEmailMessage($data['data']);
            $response->setPayload($message->toData());
        } elseif ($data['dataType'] === 'collection') {
            $messages = [];
            foreach ($data['data'] as $messageData) {
                if (strlen($messageData['body']) > $this->maxBodySizeBytes) {
                    $this->serviceLog->warn(sprintf(
                        'Message body exceeds max allowed bytes, %s: from: "%s", to: "%s", subject: "%s"',
                        $this->maxBodySizeBytes,
                        $messageData['fromAddr'],
                        $messageData['toAddr'],
                        $messageData['subject']
                    ));
                    $messages[] = [
                        'code' => App::API_BADREQUEST,
                        'message' => sprintf('Message body exceeds max allowed bytes, %s', $this->maxBodySizeBytes),
                        'model' => [],
                    ];
                    continue;
                }

                try {
                    $message = $this->emailRepository->createEmailMessage($messageData);
                    $messages[] = [
                        'code' => App::API_CREATED,
                        'message' => '',
                        'model' => $message->toData(),
                    ];
                } catch (\Exception $e) {
                    $messages[] = [
                        'code' => App::API_FAILED,
                        'message' => $e->getMessage(),
                        'model' => [],
                    ];
                }
            }
            $response->setPayload($messages);
        } else {
            throw new InvalidArgumentException(sprintf('Invalid dataType %s, must be "model" or "collection"', $data['dataType']));
        }

        $response->setStatus(App::API_CREATED);

        return $response;
    }

    public function updateEmailMessage()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'update');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $id = $request->getResourceParam('id');

        $data = $request->getData();

        try {
            $this->emailRepository->updateEmailMessage($id, $data);
        } catch (NotFoundException $e) {
            $response->setStatus(App::API_NOTFOUND);
            return $response;
        }

        $response->setStatus(App::API_OK);

        return $response;
    }

    public function updateEmailMessages()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'update');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $data = $request->getData();

        $results = $this->emailRepository->updateEmailMessages($data);

        $response->setPayload($results->toArray());
        $response->setStatus(App::API_OK);

        return $response;
    }

    public function getPendingEmailMessages()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'queue');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $options = $request->getOptions();

        $messages = $this->emailRepository->getPendingEmailMessages($options);

        $response->setPayload($messages->map(function (EmailModel $message) {
            return $message->toData();
        })->toArray());

        return $response;
    }

    public function getEmailQueueStatus()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $summary = $this->emailRepository->getEmailQueueStatus($request->getOptions());

        if ($summary['status'] === 'critical') {
            $response->setStatus(App::API_FAILED);
        }

        $response->setPayload($summary);

        return $response;
    }

    public function getEmailMessageCollection()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('Notification Service', 'Email', 'read');

        if (!$authorized) {
            $response->setStatus(App::API_UNAUTHORIZED);
            return $response;
        }

        $response->setStatus(App::API_NOTIMPLEMENTED);

        return $response;
    }
}
