<?php


namespace MiamiOH\NotificationService;


interface DataModel
{
    public function save(array $options = []);

    public function toData(): array;

    public function fromData(array $data): void;
}